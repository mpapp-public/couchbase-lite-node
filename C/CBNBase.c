#include "CBNBase.h"
#include "CBNUtilities.h"

napi_value CBNGetBuildInfo(napi_env env, napi_callback_info info) {
  if (!CBNEnsureVoidArguments(env, info, NULL)) {
    return NULL;
  }

  C4StringResult res = c4_getBuildInfo();
  napi_value str;
  if (napi_create_string_utf8(env, res.buf, res.size, &str) != napi_ok) {
    napi_throw_error(env, NULL, "Failed to create string value representation "
                                "for build info C4 string");
    return NULL;
  }
  c4slice_free(res);

  return str;
}

void c4_dumpInstances(void) C4API;

napi_value CBNGetObjectCount(napi_env env, napi_callback_info info) {
  if (!CBNEnsureVoidArguments(env, info, NULL)) {
    return NULL;
  }

  int64_t res = (int64_t)c4_getObjectCount();

  napi_value ret;
  if (napi_create_int64(env, res, &ret) != napi_ok) {
    napi_throw_error(
        env, NULL,
        "Failed to create int value representation for object count");
    return NULL;
  }

  return ret;
}

napi_value CBNDumpInstances(napi_env env, napi_callback_info info) {
  if (!CBNEnsureVoidArguments(env, info, NULL)) {
    return NULL;
  }

  c4_dumpInstances();
  return NULL;
}

napi_value CBNCreateDefaultLogDomains(napi_env env) {
  napi_value log_domain;
  if (napi_create_object(env, &log_domain) != napi_ok) {
    napi_throw_error(env, NULL, "Failed to create log domain object");
    return NULL;
  }

  napi_value default_log_domain_ext, database_log_domain_ext,
      query_log_domain_ext, sync_log_domain_ext, websocket_log_domain_ext;

  if (napi_create_external(env, kC4DefaultLog, NULL, NULL,
                           &default_log_domain_ext) != napi_ok ||
      napi_create_external(env, kC4DatabaseLog, NULL, NULL,
                           &database_log_domain_ext) != napi_ok ||
      napi_create_external(env, kC4QueryLog, NULL, NULL,
                           &query_log_domain_ext) != napi_ok ||
      napi_create_external(env, kC4SyncLog, NULL, NULL, &sync_log_domain_ext) !=
          napi_ok ||
      napi_create_external(env, kC4WebSocketLog, NULL, NULL,
                           &websocket_log_domain_ext) != napi_ok) {
    napi_throw_error(env, NULL, "Failed to create log domain externals");
    return NULL;
  }

  if (napi_set_named_property(env, log_domain, "kC4DefaultLog",
                              default_log_domain_ext) != napi_ok ||
      napi_set_named_property(env, log_domain, "kC4DatabaseLog",
                              database_log_domain_ext) != napi_ok ||
      napi_set_named_property(env, log_domain, "kC4QueryLog",
                              query_log_domain_ext) != napi_ok ||
      napi_set_named_property(env, log_domain, "kC4SyncLog",
                              sync_log_domain_ext) != napi_ok ||
      napi_set_named_property(env, log_domain, "kC4WebSocketLog",
                              websocket_log_domain_ext) != napi_ok) {
    napi_throw_error(env, NULL, "Failed to set domain constant properties");
    return NULL;
  }

  return log_domain;
}

napi_value CBNCreateLogLevels(napi_env env) {
  napi_value log_levels;
  if (napi_create_object(env, &log_levels) != napi_ok) {
    napi_throw_error(env, NULL, "Failed to create log levels object");
    return NULL;
  }

  napi_value kC4LogDebug_ext, kC4LogVerbose_ext, kC4LogInfo_ext,
      kC4LogWarning_ext, kC4LogError_ext, kC4LogNone_ext;

  if (napi_create_int32(env, kC4LogDebug, &kC4LogDebug_ext) != napi_ok ||
      napi_create_int32(env, kC4LogVerbose, &kC4LogVerbose_ext) != napi_ok ||
      napi_create_int32(env, kC4LogInfo, &kC4LogInfo_ext) != napi_ok ||
      napi_create_int32(env, kC4LogWarning, &kC4LogWarning_ext) != napi_ok ||
      napi_create_int32(env, kC4LogError, &kC4LogError_ext) != napi_ok ||
      napi_create_int32(env, kC4LogNone, &kC4LogNone_ext) != napi_ok) {
    napi_throw_error(env, NULL, "Failed to create log level constant values");
    return NULL;
  }

  if (napi_set_named_property(env, log_levels, "kC4LogDebug",
                              kC4LogDebug_ext) != napi_ok ||
      napi_set_named_property(env, log_levels, "kC4LogVerbose",
                              kC4LogVerbose_ext) != napi_ok ||
      napi_set_named_property(env, log_levels, "kC4LogInfo", kC4LogInfo_ext) !=
          napi_ok ||
      napi_set_named_property(env, log_levels, "kC4LogWarning",
                              kC4LogWarning_ext) != napi_ok ||
      napi_set_named_property(env, log_levels, "kC4LogError",
                              kC4LogError_ext) != napi_ok ||
      napi_set_named_property(env, log_levels, "kC4LogNone", kC4LogNone_ext) !=
          napi_ok) {
    napi_throw_error(env, NULL, "Failed to set log level constant properties");
    return NULL;
  }

  return log_levels;
}

napi_value CBNLogGetDomain(napi_env env, napi_callback_info info) {

  napi_value argv[2];
  size_t argc = 2;
  if (napi_get_cb_info(env, info, &argc, argv, NULL, NULL) != napi_ok) {
    napi_throw_error(env, NULL, "Failed to parse arguments");
    return NULL;
  }

  if (argc != 2) {
    napi_throw_error(env, NULL, "Unexpected number of arguments (expected 2).");
    return NULL;
  }

  bool create;
  if (napi_get_value_bool(env, argv[1], &create) != napi_ok) {
    napi_throw_error(env, NULL,
                     "Failed to get value for 'create' from input arguments");
    return NULL;
  }
  char *domain_str = CBNCStringForStringValue(env, argv[0]);
  C4LogDomain log_domain = c4log_getDomain(domain_str, create);
  free(domain_str);

  napi_value ext;
  if (napi_create_external(env, log_domain, NULL, NULL, &ext) != napi_ok) {
    napi_throw_error(env, NULL,
                     "Creating external to wrap retrieved log domain failed");
    return NULL;
  }

  return ext;
}

napi_value CBNLogGetDomainName(napi_env env, napi_callback_info info) {
  napi_value argv[1];
  size_t argc = 1;
  if (napi_get_cb_info(env, info, &argc, argv, NULL, NULL) != napi_ok) {
    napi_throw_error(env, NULL, "Failed to parse arguments");
    return NULL;
  }

  if (argc != 1) {
    napi_throw_error(env, NULL, "Unexpected number of arguments (expected 1).");
    return NULL;
  }

  C4LogDomain log_domain = NULL;
  if (napi_get_value_external(env, argv[0], (void **)&log_domain) != napi_ok) {
    napi_throw_error(env, NULL,
                     "Failed to decode log domain value from 1st argument.");
    return NULL;
  }

  const char *name = c4log_getDomainName(log_domain);
  return CBNStringValueForString(env, c4str(name));
}

napi_value CBNLogGetLevel(napi_env env, napi_callback_info info) {
  napi_value argv[1];
  size_t argc = 1;
  if (napi_get_cb_info(env, info, &argc, argv, NULL, NULL) != napi_ok) {
    napi_throw_error(env, NULL, "Failed to parse arguments");
    return NULL;
  }

  if (argc != 1) {
    napi_throw_error(env, NULL, "Unexpected number of arguments (expected 1).");
    return NULL;
  }

  C4LogDomain log_domain = NULL;
  if (napi_get_value_external(env, argv[0], (void **)&log_domain) != napi_ok) {
    napi_throw_error(env, NULL,
                     "Failed to decode log domain value from 1st argument.");
    return NULL;
  }

  C4LogLevel log_level = c4log_getLevel(log_domain);

  napi_value log_level_obj;
  if (napi_create_int32(env, log_level, &log_level_obj) != napi_ok) {
    napi_throw_error(env, NULL, "Failed to wrap log level to an int32 value");
    return NULL;
  }

  return log_level_obj;
}

napi_value CBNLogSetLevel(napi_env env, napi_callback_info info) {
  napi_value argv[2];
  size_t argc = 2;
  if (napi_get_cb_info(env, info, &argc, argv, NULL, NULL) != napi_ok) {
    napi_throw_error(env, NULL, "Failed to parse arguments");
    return NULL;
  }

  if (argc != 2) {
    napi_throw_error(env, NULL, "Unexpected number of arguments (expected 2).");
    return NULL;
  }

  C4LogDomain log_domain = NULL;
  if (napi_get_value_external(env, argv[0], (void **)&log_domain) != napi_ok) {
    napi_throw_error(env, NULL,
                     "Failed to decode log domain value from argument.");
    return NULL;
  }

  int32_t log_level;
  if (napi_get_value_int32(env, argv[1], &log_level)) {
    napi_throw_error(env, NULL, "Failed to decode log level from 2nd argument");
    return NULL;
  }

  if (log_level > 255 || log_level < 0) {
    napi_throw_error(env, NULL, "Unexpected log level");
    return NULL;
  }

  c4log_setLevel(log_domain, (C4LogLevel)log_level);
  return NULL;
}