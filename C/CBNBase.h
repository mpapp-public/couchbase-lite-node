#ifndef CBNBASE_H
#define CBNBASE_H

#include <c4Base.h>
#include <node_api.h>

napi_value CBNGetBuildInfo(napi_env env, napi_callback_info info);
napi_value CBNGetObjectCount(napi_env env, napi_callback_info info);
napi_value CBNDumpInstances(napi_env env, napi_callback_info info);

napi_value CBNCreateDefaultLogDomains(napi_env env);
napi_value CBNCreateLogLevels(napi_env env);

napi_value CBNLogGetDomainName(napi_env env, napi_callback_info info);
napi_value CBNLogGetDomain(napi_env env, napi_callback_info info);
napi_value CBNLogGetLevel(napi_env env, napi_callback_info info);
napi_value CBNLogSetLevel(napi_env env, napi_callback_info info);

#endif