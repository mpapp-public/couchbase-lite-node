#include "CBNDatabase.h"
#include "CBNUtilities.h"

C4Database *CBNDatabaseForValue(napi_env env, napi_value value) {
  bool has_db = false;
  if (napi_has_named_property(env, value, "_db", &has_db) != napi_ok) {
    napi_throw_error(env, NULL, "Object passed in as argument doesn't appear "
                                "to wrap a database handle.");
    return NULL;
  }

  napi_value db_external;
  if (napi_get_named_property(env, value, "_db", &db_external) != napi_ok) {
    napi_throw_error(env, NULL,
                     "Failed to get property _db of value given as argument.");
    return NULL;
  }

  C4Database *db = NULL;
  if (napi_get_value_external(env, db_external, (void **)&db) != napi_ok) {
    napi_throw_error(env, NULL,
                     "Failed to unpack database handle from external value.");
    return NULL;
  }

  return db;
}

C4Database *CBNEnsureValidDatabaseHandle(napi_env env, napi_callback_info info,
                                         size_t expected_argc,
                                         napi_value *out_argv) {
  // stack smashing can occur if C4Database APIs are called with larger numbers
  // of arguments than this -- hence allocating a large buffer here.
  napi_value argv[32];

  size_t argc = expected_argc;
  if (napi_get_cb_info(env, info, &argc, argv, NULL, NULL) != napi_ok) {
    napi_throw_error(env, NULL, "Failed to parse arguments");
    return NULL;
  }

  if (argc != expected_argc) {
    napi_throw_error(env, NULL, "Unexpected number of arguments (expected 2).");
    return NULL;
  }

  if (out_argv) {
    // FIXME: Should really copy a max of 32 napi_values, because argv above is
    // of fixed size 32 * sizeof(napi_value).
    memcpy(out_argv, argv, sizeof(napi_value) * argc);
  }

  C4Database *db = CBNDatabaseForValue(env, argv[0]);
  return db;
}

napi_value CBNThrowDatabaseError(napi_env env, C4Error error, const char *msg) {
  napi_value error_code_val;

  char code[64];
  sprintf(code, "%u-%u", error.domain, error.code);

  if (napi_create_string_utf8(env, code, strlen(code), &error_code_val) !=
      napi_ok) {
    napi_throw_error(env, NULL, "Failed to create database error code value.");
    return NULL;
  }

  char c4_msg[2048];
  c4error_getMessageC(error, c4_msg, 2048);

  char whole_msg[4096];
  sprintf(whole_msg, "%s: %s", msg, c4_msg);

  napi_value error_msg_val;
  if (napi_create_string_utf8(env, whole_msg, strlen(whole_msg),
                              &error_msg_val) != napi_ok) {
    napi_throw_error(env, NULL,
                     "Failed to create database error message value.");
    return NULL;
  }
  napi_value error_val;
  if (napi_create_error(env, error_code_val, error_msg_val, &error_val)) {
    napi_throw_error(env, NULL, "Failed to  create database error value.");
    return NULL;
  }
  napi_throw(env, error_val);
  return error_val;
}

void finalize_db_external(napi_env env, void *finalize_data,
                          void *finalize_hint) {
  C4Database *db = (C4Database *)finalize_data;

  C4Error error;
  if (!c4db_close(db, &error)) {
    fprintf(stderr, "Database close failed: %d (domain: %d)\n", error.code,
            error.domain);
  }
  c4db_free(finalize_data);
}

void finalize_doc_external(napi_env env, void *finalize_data,
                           void *finalize_hint) {
  C4RawDocument *doc = (C4RawDocument *)finalize_data;
  c4raw_free(doc);
}

bool CBNGetDatabaseConfiguration(napi_env env, napi_value value,
                                 C4DatabaseConfig *returned_config) {
  napi_value flags_value, storage_engine_value, versioning_value,
      encryption_key_value;
  if (napi_get_named_property(env, value, "flags", &flags_value) != napi_ok) {
    napi_throw_error(env, NULL, "No 'flags' field in object passed in as 2nd "
                                "argument (configuration).");
    return false;
  }
  if (napi_get_named_property(env, value, "storageEngine",
                              &storage_engine_value) != napi_ok) {
    napi_throw_error(env, NULL, "No 'storageEngine' field in object passed in "
                                "as 2nd argument (configuration).");
    return false;
  }

  if (napi_get_named_property(env, value, "versioning", &versioning_value) !=
      napi_ok) {
    napi_throw_error(env, NULL, "No 'versioning' field in object passed in as "
                                "2nd argument (configuration).");
    return false;
  }
  if (napi_get_named_property(env, value, "encryptionKey",
                              &encryption_key_value) != napi_ok) {
    napi_throw_error(env, NULL, "No 'encryptionKey' field in object passed in "
                                "as 2nd argument (configuration).");
    return false;
  }

  int64_t flags;
  if (napi_get_value_int64(env, flags_value, &flags) != napi_ok) {
    napi_throw_error(env, NULL, "Value for 'flags' is not an integer.");
    return false;
  }

  static char storageEngine[128];
  if (napi_get_value_string_utf8(env, storage_engine_value, storageEngine, 0,
                                 NULL) != napi_ok) {
    napi_throw_error(env, NULL, "Value for 'storageEngine' is not a string.");
    return false;
  }

  int32_t versioning;
  if (napi_get_value_int32(env, versioning_value, &versioning) != napi_ok) {
    napi_throw_error(env, NULL, "Value for 'versioning' is not an integer.");
    return false;
  }

  napi_value algorithm_value;
  if (napi_get_named_property(env, encryption_key_value, "algorithm",
                              &algorithm_value) != napi_ok) {
    napi_throw_error(env, NULL, "Value for 'algorithm' is not available.");
    return false;
  }

  int32_t algorithm;
  if (napi_get_value_int32(env, algorithm_value, &algorithm) != napi_ok) {
    napi_throw_error(env, NULL, "Value for 'algorithm' is not an integer.");
    return false;
  }

  napi_value bytes_value;
  if (napi_get_named_property(env, encryption_key_value, "bytes",
                              &bytes_value) != napi_ok) {
    napi_throw_error(
        env, NULL,
        "Value for 'bytes' is missing in object passed in as 3rd argument.");
    return false;
  }

  void *bytes = NULL;
  size_t bytes_size;
  if (napi_get_buffer_info(env, bytes_value, &bytes, &bytes_size) != napi_ok) {
    napi_throw_error(env, NULL, "Value for 'bytes' is not a buffer.");
    return false;
  }

  if (bytes_size != 32) {
    napi_throw_error(env, NULL, "Encryption key has unexpected length.");
    return false;
  }

  C4EncryptionKey encryptionKey;
  encryptionKey.algorithm = algorithm;
  memcpy(encryptionKey.bytes, bytes, 32);

  C4DatabaseConfig config;
  config.flags = flags;
  config.storageEngine = storageEngine;
  config.versioning = versioning;
  config.encryptionKey = encryptionKey;

  if (returned_config) {
    *returned_config = config;
  }

  return true;
}

napi_value CNBCreateDatabaseFlags(napi_env env) {
  napi_value flags;
  napi_create_object(env, &flags);

  napi_value kC4DB_Create_val, kC4DB_ReadOnly_val, kC4DB_AutoCompact_val,
      kC4DB_SharedKeys_val, kC4DB_NoUpgrade_val, kC4DB_NonObservable_val;
  if (napi_create_int32(env, kC4DB_Create, &kC4DB_Create_val) != napi_ok ||
      napi_create_int32(env, kC4DB_ReadOnly, &kC4DB_ReadOnly_val) != napi_ok ||
      napi_create_int32(env, kC4DB_AutoCompact, &kC4DB_AutoCompact_val) !=
          napi_ok ||
      napi_create_int32(env, kC4DB_SharedKeys, &kC4DB_SharedKeys_val) !=
          napi_ok ||

      napi_create_int32(env, kC4DB_NoUpgrade, &kC4DB_NoUpgrade_val) !=
          napi_ok ||
      napi_create_int32(env, kC4DB_NonObservable, &kC4DB_NonObservable_val) !=
          napi_ok) {
    napi_throw_error(env, NULL, "Creating database flags failed");
    return NULL;
  }

  napi_set_named_property(env, flags, "kC4DB_Create", kC4DB_Create_val);
  napi_set_named_property(env, flags, "kC4DB_ReadOnly", kC4DB_ReadOnly_val);
  napi_set_named_property(env, flags, "kC4DB_AutoCompact",
                          kC4DB_AutoCompact_val);
  napi_set_named_property(env, flags, "kC4DB_SharedKeys", kC4DB_SharedKeys_val);
  napi_set_named_property(env, flags, "kC4DB_NoUpgrade", kC4DB_NoUpgrade_val);
  napi_set_named_property(env, flags, "kC4DB_NonObservable",
                          kC4DB_NonObservable_val);

  return flags;
}

// C4Database* c4db_open(C4Slice path, const C4DatabaseConfig *configP,
// *outError)
// c4db_open(path: string, configP: Object)
// configuration of shape
// {
//   flags: C4DatabaseFlags (options mask, with value 1 saying "create"),
//   storageEngine: string with value of kC4SQLiteStorageEngine ("SQLite"),
//   versioning: kC4RevisionTrees or kC4VersionVectors,
//   encryptionKey: { algorithm: kC4EncryptionNone or kC4EncryptionAES256,
//   bytes: }
// }
napi_value CBNOpenDatabase(napi_env env, napi_callback_info info) {
  size_t argc = 2;
  napi_value argv[2];

  if (napi_get_cb_info(env, info, &argc, argv, NULL, NULL) != napi_ok) {
    napi_throw_error(env, NULL, "Failed to parse arguments");
    return NULL;
  }

  if (argc != 2) {
    napi_throw_error(env, NULL, "Unexpected number of arguments (expected 2).");
    return NULL;
  }

  // path
  static char path[2048];
  size_t path_len = 0;
  if ((napi_get_value_string_utf8(env, argv[0], path, 2048, &path_len) !=
       napi_ok) ||
      (path[0] == '\0')) {

    napi_throw_error(env, NULL, "Failed to read database path");
    return NULL;
  }

  // configuration

  C4DatabaseConfig config;
  if (!CBNGetDatabaseConfiguration(env, argv[1], &config)) {
    return NULL;
  }

  C4Error error;
  C4Database *db = NULL;
  char *out_path = malloc((path_len + 1) * sizeof(char));
  strncpy(out_path, path, path_len + 1);
  if (!(db = c4db_open(c4str(out_path), &config, &error))) {
    CBNThrowDatabaseError(env, error, "Failed to open database.");
    return NULL;
  }

  napi_value db_obj;
  if (napi_create_object(env, &db_obj) != napi_ok) {
    napi_throw_error(env, NULL, "Failed to create return value.");
    return NULL;
  }

  napi_value db_external = NULL;

  if (napi_create_external(env, db, finalize_db_external, NULL, &db_external) !=
      napi_ok) {
    napi_throw_error(env, NULL, "Failed to wrap object in an external.");
    return NULL;
  }

  if (napi_set_named_property(env, db_obj, "_db", db_external) != napi_ok) {
    napi_throw_error(env, NULL, "Failing to assign the value of the database "
                                "object reference in a JS object.");
    return NULL;
  }

  return db_obj;
}

napi_value CBNOpenDatabaseAgain(napi_env env, napi_callback_info ninfo) {
  C4Database *db = CBNEnsureValidDatabaseHandle(env, ninfo, 1, NULL);

  C4Error error;
  if (!(db = c4db_openAgain(db, &error))) {
    CBNThrowDatabaseError(env, error, "Failed to open database again.");
    return NULL;
  }

  napi_value db_obj;
  if (napi_create_object(env, &db_obj) != napi_ok) {
    napi_throw_error(env, NULL, "Failed to create return value.");
    return NULL;
  }

  napi_value db_external = NULL;
  if (napi_create_external(env, db, finalize_db_external, NULL, &db_external) !=
      napi_ok) {
    napi_throw_error(env, NULL, "Failed to wrap object in an external.");
    return NULL;
  }

  if (napi_set_named_property(env, db_obj, "_db", db_external) != napi_ok) {
    napi_throw_error(env, NULL, "Failing to assign the value of the database "
                                "object reference in a JS object.");
    return NULL;
  }

  return db_obj;
}

napi_value CBNCopyDatabase(napi_env env, napi_callback_info info) {
  size_t argc = 3;
  napi_value argv[3];

  if (napi_get_cb_info(env, info, &argc, argv, NULL, NULL) != napi_ok) {
    napi_throw_error(env, NULL, "Failed to parse arguments");
    return CBNBoolean(env, false);
  }

  if (argc != 3) {
    napi_throw_error(env, NULL, "Unexpected number of arguments (expected 3).");
    return CBNBoolean(env, false);
  }

  C4DatabaseConfig config;
  if (!CBNGetDatabaseConfiguration(env, argv[2], &config)) {
    return CBNBoolean(env, false);
  }
  C4Error error;
  C4String source_path = CBNStringForStringValue(env, argv[0]);
  C4String target_path = CBNStringForStringValue(env, argv[1]);

  bool success = c4db_copy(source_path, target_path, &config, &error);
  if (!success) {
    CBNThrowDatabaseError(env, error, "Failed to copy database");
  }

  CBNFreeString(source_path);
  CBNFreeString(target_path);
  return CBNBoolean(env, success);
}

napi_value CBNGetDocumentCount(napi_env env, napi_callback_info info) {
  C4Database *db = CBNEnsureValidDatabaseHandle(env, info, 1, NULL);
  if (!db) {
    return NULL;
  }

  int doc_count = c4db_getDocumentCount(db);

  napi_value doc_count_val;
  if (napi_create_int32(env, doc_count, &doc_count_val) != napi_ok) {
    napi_throw_error(
        env, NULL,
        "Failed to create int32 value representing database document count.");
    return NULL;
  }

  return doc_count_val;
}

napi_value CBNCloseDatabase(napi_env env, napi_callback_info info) {
  C4Database *db = CBNEnsureValidDatabaseHandle(env, info, 1, NULL);

  C4Error error;
  if (!c4db_close(db, &error)) {
    napi_throw_error(env, NULL, "Failed to close database.");
    return CBNBoolean(env, false);
  }

  return CBNBoolean(env, true);
}

napi_value CBNDeleteDatabase(napi_env env, napi_callback_info info) {
  C4Database *db = CBNEnsureValidDatabaseHandle(env, info, 1, NULL);

  C4Error error;
  if (!c4db_delete(db, &error)) {
    CBNThrowDatabaseError(env, error, "Failed to delete database.");
    return CBNBoolean(env, false);
  }

  return CBNBoolean(env, true);
}

napi_value CBNDeleteDatabaseAtPath(napi_env env, napi_callback_info info) {
  size_t argc = 1;
  napi_value argv[1];

  if (napi_get_cb_info(env, info, &argc, argv, NULL, NULL) != napi_ok) {
    napi_throw_error(env, NULL, "Failed to parse arguments");
    return CBNBoolean(env, false);
  }

  if (argc != 1) {
    napi_throw_error(env, NULL, "Unexpected number of arguments (expected 1).");
    return CBNBoolean(env, false);
  }

  static char path[1024];
  if (napi_get_value_string_utf8(env, argv[0], path, 0, NULL) != napi_ok) {
    napi_throw_error(env, NULL, "Failed to read 'sourcePath' argument.");
    return CBNBoolean(env, false);
  }

  C4Error error;
  if (!c4db_deleteAtPath(c4str(path), &error)) {
    CBNThrowDatabaseError(env, error, "Failed to delete database at path.");
  }

  return CBNBoolean(env, true);
}

napi_value CBNShutdown(napi_env env, napi_callback_info info) {
  size_t argc = 0;
  napi_value argv[1];
  if (napi_get_cb_info(env, info, &argc, argv, NULL, NULL) != napi_ok) {
    napi_throw_error(env, NULL, "Failed to parse arguments");
    return CBNBoolean(env, false);
  }

  if (argc != 0) {
    napi_throw_error(env, NULL,
                     "Unexpected number of arguments (expected none).");
    return CBNBoolean(env, false);
  }

  C4Error error;
  if (!c4_shutdown(&error)) {
    CBNThrowDatabaseError(env, error,
                          "Failed to shutdown Couchbase Lite Core.");
    return CBNBoolean(env, false);
  }

  return CBNBoolean(env, true);
}

napi_value CBNGetDatabasePath(napi_env env, napi_callback_info info) {
  C4Database *db = CBNEnsureValidDatabaseHandle(env, info, 1, NULL);
  C4StringResult path = c4db_getPath(db);

  napi_value result;
  if (napi_create_string_utf8(env, path.buf, path.size, &result) != napi_ok) {
    c4slice_free(path);
    napi_throw_error(env, NULL,
                     "Failed to create string value to store database path.");
    return NULL;
  }
  c4slice_free(path);

  return result;
}

napi_value CBNGetLastSequence(napi_env env, napi_callback_info info) {
  C4Database *db = CBNEnsureValidDatabaseHandle(env, info, 1, NULL);
  C4SequenceNumber sn = c4db_getLastSequence(db);

  napi_value result;
  // TODO: Work out if there's something that can be done with the sequence
  // number being uint64 and this being int64?
  if (napi_create_int64(env, sn, &result) != napi_ok) {
    napi_throw_error(env, NULL,
                     "Failed to create int64 value to store database path.");
    return NULL;
  }

  return result;
}

// uint64_t c4db_nextDocExpiration(C4Database *database C4NONNULL) C4API;
napi_value CBNNextDocExpiration(napi_env env, napi_callback_info info) {
  C4Database *db = CBNEnsureValidDatabaseHandle(env, info, 1, NULL);

  uint64_t expiry = c4db_nextDocExpiration(db);

  napi_value result;
  if (napi_create_int64(env, expiry, &result) != napi_ok) {
    napi_throw_error(
        env, NULL,
        "Failed to create int64 value to store document expiration.");
    return NULL;
  }

  return result;
}

// uint32_t c4db_getMaxRevTreeDepth(C4Database *database C4NONNULL) C4API;
napi_value CBNGetMaxRevTreeDepth(napi_env env, napi_callback_info info) {
  C4Database *db = CBNEnsureValidDatabaseHandle(env, info, 1, NULL);

  uint64_t max_depth = c4db_getMaxRevTreeDepth(db);

  napi_value result;
  if (napi_create_int64(env, max_depth, &result) != napi_ok) {
    napi_throw_error(
        env, NULL, "Failed to create int64 value to store max rev tree depth.");
  }

  return result;
}

// void c4db_setMaxRevTreeDepth(C4Database *database C4NONNULL, uint32_t
// maxRevTreeDepth) C4API;
napi_value CBNSetMaxRevTreeDepth(napi_env env, napi_callback_info info) {
  size_t argc = 2;
  napi_value argv[2];

  if (napi_get_cb_info(env, info, &argc, argv, NULL, NULL) != napi_ok) {
    napi_throw_error(env, NULL, "Failed to parse arguments");
    return NULL;
  }

  if (argc != 2) {
    napi_throw_error(env, NULL, "Unexpected number of arguments (expected 2).");
    return NULL;
  }

  C4Database *db = CBNDatabaseForValue(env, argv[0]);

  int64_t max_rev_tree_depth = 0;
  if (napi_get_value_int64(env, argv[1], &max_rev_tree_depth) != napi_ok) {
    napi_throw_error(env, NULL, "Failed to get int64 value from 2nd argument.");
    return NULL;
  }

  c4db_setMaxRevTreeDepth(db, max_rev_tree_depth);

  return NULL;
}

// bool c4db_getUUIDs(C4Database* database C4NONNULL,
//    C4UUID *publicUUID, C4UUID *privateUUID,
//    C4Error *outError) C4API;
napi_value CBNGetDatabaseUUIDs(napi_env env, napi_callback_info info) {
  C4Database *db = CBNEnsureValidDatabaseHandle(env, info, 1, NULL);

  C4UUID publicUUID;
  C4UUID privateUUID;
  C4Error error;
  if (!c4db_getUUIDs(db, &publicUUID, &privateUUID, &error)) {
    CBNThrowDatabaseError(env, error, "Failed to get database UUIDs.");
    return NULL;
  }

  napi_value obj, privateUUIDObj, publicUUIDObj;
  if (napi_create_object(env, &obj) != napi_ok ||
      napi_create_object(env, &privateUUIDObj) != napi_ok ||
      napi_create_object(env, &publicUUIDObj) != napi_ok) {
    napi_throw_error(
        env, NULL, "Failed to create object to wrap public and private UUIDs.");
    return NULL;
  }

  napi_value privateUUIDBuffer, publicUUIDBuffer;
  if (napi_create_buffer(env, 32, (void **)&publicUUID.bytes,
                         &publicUUIDBuffer) ||
      napi_create_buffer(env, 32, (void **)&privateUUID.bytes,
                         &privateUUIDBuffer)) {
    napi_throw_error(env, NULL, "Failed to create buffer for storing UUIDs.");
    return NULL;
  }

  if (napi_set_named_property(env, privateUUIDObj, "bytes",
                              privateUUIDBuffer) != napi_ok ||
      napi_set_named_property(env, publicUUIDObj, "bytes", publicUUIDBuffer) !=
          napi_ok) {
    napi_throw_error(
        env, NULL,
        "Failed to set public / private UUIDs on the object to be returned.");
    return NULL;
  }

  if (napi_set_named_property(env, obj, "privateUUID", privateUUIDObj) !=
          napi_ok ||
      napi_set_named_property(env, obj, "publicUUID", publicUUIDObj) !=
          napi_ok) {
    napi_throw_error(env, NULL, "Failed to set privateUUID / publicUUID fields "
                                "on the object to be returned.");
    return NULL;
  }

  return obj;
}

napi_value CBNCompactDatabase(napi_env env, napi_callback_info info) {
  C4Database *db = CBNEnsureValidDatabaseHandle(env, info, 1, NULL);

  C4Error error;
  if (!c4db_compact(db, &error)) {
    CBNThrowDatabaseError(env, error, "Failed to compact database.");
    return CBNBoolean(env, false);
  }

  return CBNBoolean(env, true);
}

napi_value CBNBeginTransaction(napi_env env, napi_callback_info info) {
  C4Database *db = CBNEnsureValidDatabaseHandle(env, info, 1, NULL);

  C4Error error;
  if (!c4db_beginTransaction(db, &error)) {
    CBNThrowDatabaseError(env, error, "Failed to begin transaction.");
    return CBNBoolean(env, false);
  }

  return CBNBoolean(env, true);
}

napi_value CBNEndTransaction(napi_env env, napi_callback_info info) {
  napi_value argv[2];
  C4Database *db = CBNEnsureValidDatabaseHandle(env, info, 2, argv);

  bool commit;
  if (napi_get_value_bool(env, argv[1], &commit) != napi_ok) {
    napi_throw_error(env, NULL, "Failed to get value for 'commit' boolean.");
    return CBNBoolean(env, false);
  }

  C4Error error;
  if (!c4db_endTransaction(db, commit, &error)) {
    CBNThrowDatabaseError(env, error, "Failed to end transaction.");
    return CBNBoolean(env, false);
  }

  return CBNBoolean(env, true);
}

napi_value CBNIsInTransaction(napi_env env, napi_callback_info info) {
  C4Database *db = CBNEnsureValidDatabaseHandle(env, info, 1, NULL);
  bool in_transaction = c4db_isInTransaction(db);
  return CBNBoolean(env, in_transaction);
}

C4RawDocument *CBNEnsureValidRawDocument(napi_env env,
                                         napi_callback_info info) {
  size_t argc = 0;
  napi_value argv[16];
  napi_value this;
  C4RawDocument *raw_doc;

  void *data;
  if (napi_get_cb_info(env, info, &argc, argv, &this, &data) != napi_ok) {
    napi_throw_error(env, NULL, "Failed to get callback info.");
    return NULL;
  }

  if (argc != 0) {
    napi_throw_error(env, NULL, "Unexpected number of arguments");
    return NULL;
  }

  if (!this) {
    napi_throw_error(env, NULL,
                     "GetRawDocument function called without this being bound");
    return NULL;
  }

  napi_value doc_wrapper;
  if (napi_get_named_property(env, this, "_doc", &doc_wrapper) != napi_ok) {
    napi_throw_error(env, NULL, "Failed to read named property _doc");
    return NULL;
  }

  if (napi_get_value_external(env, doc_wrapper, (void **)&raw_doc) != napi_ok) {
    napi_throw_error(env, NULL, "Failed to decode external from value.");
    return NULL;
  }

  return raw_doc;
}

napi_value CBNGetRawDocumentKey(napi_env env, napi_callback_info info) {
  C4RawDocument *raw_doc = CBNEnsureValidRawDocument(env, info);
  C4String key = raw_doc->key;
  napi_value out;
  napi_create_string_utf8(env, key.buf, key.size, &out);
  return out;
}

napi_value CBNGetRawDocumentMeta(napi_env env, napi_callback_info info) {
  C4RawDocument *raw_doc = CBNEnsureValidRawDocument(env, info);
  C4String key = raw_doc->meta;
  napi_value out;
  napi_create_string_utf8(env, key.buf, key.size, &out);
  return out;
}

napi_value CBNGetRawDocumentBody(napi_env env, napi_callback_info info) {
  C4RawDocument *raw_doc = CBNEnsureValidRawDocument(env, info);
  C4String key = raw_doc->body;
  napi_value out;
  napi_create_string_utf8(env, key.buf, key.size, &out);
  return out;
}

napi_value CBNGetRawDocument(napi_env env, napi_callback_info info) {
  napi_value argv[3];
  C4Database *db = CBNEnsureValidDatabaseHandle(env, info, 3, argv);

  C4String storeName = CBNStringForStringValue(env, argv[1]);
  if (storeName.size == 0) {
    CBNFreeString(storeName);
    return NULL;
  }

  C4String docID = CBNStringForStringValue(env, argv[2]);
  if (docID.size == 0) {
    CBNFreeString(storeName);
    CBNFreeString(docID);
    return NULL;
  }

  C4RawDocument *doc = NULL;
  C4Error error;

  if (!(doc = c4raw_get(db, storeName, docID, &error))) {
    CBNFreeString(storeName);
    CBNFreeString(docID);
    CBNThrowDatabaseError(env, error, "Failed to get raw document");
    return NULL;
  }
  CBNFreeString(storeName);
  CBNFreeString(docID);

  napi_value obj;
  if (napi_create_object(env, &obj) != napi_ok) {
    napi_throw_error(env, NULL,
                     "Failed to create object wrapping a raw document.");
    return NULL;
  }

  napi_value doc_external = NULL;

  if (napi_create_external(env, doc, finalize_doc_external, NULL,
                           &doc_external) != napi_ok) {
    napi_throw_error(env, NULL, "Failed to wrap raw document in an external.");
    return NULL;
  }

  if (napi_set_named_property(env, obj, "_doc", doc_external) != napi_ok) {
    napi_throw_error(env, NULL, "Failing to assign the value of an external "
                                "referencing a raw document object reference "
                                "in a JS object.");
    return NULL;
  }

  // C4String key;    ///< The key (document ID)
  // C4String meta;   ///< Metadata (usage is up to the caller)
  // C4String body;   ///< Body data

  napi_value get_raw_document_key_fn, get_raw_document_meta_fn,
      get_raw_document_body_fn;
  napi_create_function(env, NULL, 0, CBNGetRawDocumentKey, doc,
                       &get_raw_document_key_fn);
  napi_set_named_property(env, obj, "key", get_raw_document_key_fn);
  napi_create_function(env, NULL, 0, CBNGetRawDocumentMeta, doc,
                       &get_raw_document_meta_fn);
  napi_set_named_property(env, obj, "meta", get_raw_document_meta_fn);
  napi_create_function(env, NULL, 0, CBNGetRawDocumentBody, doc,
                       &get_raw_document_body_fn);
  napi_set_named_property(env, obj, "body", get_raw_document_body_fn);

  return obj;
}

/** Frees the storage occupied by a raw document. */
// void c4raw_free(C4RawDocument* rawDoc) C4API;

napi_value CBNPutRawDocument(napi_env env, napi_callback_info info) {
  napi_value argv[5];
  C4Database *db = CBNEnsureValidDatabaseHandle(env, info, 5, argv);

  C4String store_name = CBNStringForStringValue(env, argv[1]);
  C4String key = CBNStringForStringValue(env, argv[2]);
  C4String meta = CBNStringForStringValue(env, argv[3]);
  C4String body = CBNStringForStringValue(env, argv[4]);

  C4Error error;
  bool success = false;
  if (!(success = c4raw_put(db, store_name, key, meta, body, &error))) {
    CBNThrowDatabaseError(env, error, "Failed to put raw document");
  }
  CBNFreeString(store_name);
  CBNFreeString(key);
  CBNFreeString(meta);
  CBNFreeString(body);
  return CBNBoolean(env, success);
}

napi_value CBNCreateDocumentVersioning(napi_env env) {
  napi_value doc_versioning;
  if (napi_create_object(env, &doc_versioning) != napi_ok) {
    return NULL;
  }

  napi_value kC4RevisionTrees_val, kC4VersionVectors_val;
  if (napi_create_int32(env, kC4RevisionTrees, &kC4RevisionTrees_val) !=
          napi_ok ||
      napi_create_int32(env, kC4VersionVectors, &kC4VersionVectors_val) !=
          napi_ok) {
    napi_throw_error(
        env, NULL,
        "Failed to create int32 constants for document versioning values");
    return NULL;
  }

  if (napi_set_named_property(env, doc_versioning, "kC4RevisionTrees",
                              kC4RevisionTrees_val) != napi_ok ||
      napi_set_named_property(env, doc_versioning, "kC4VersionVectors",
                              kC4VersionVectors_val) != napi_ok) {
    napi_throw_error(env, NULL,
                     "Failed to set document versioning constant values");
    return NULL;
  }

  return doc_versioning;
}

napi_value CBNCreateEncryptionAlgorithm(napi_env env) {
  napi_value encryption_algorithm;
  if (napi_create_object(env, &encryption_algorithm) != napi_ok) {
    return NULL;
  }

  napi_value kC4EncryptionNone_val, kC4EncryptionAES256_val;
  if (napi_create_int32(env, kC4EncryptionNone, &kC4EncryptionNone_val) !=
          napi_ok ||
      napi_create_int32(env, kC4EncryptionAES256, &kC4EncryptionAES256_val) !=
          napi_ok) {
    napi_throw_error(
        env, NULL,
        "Failed to create int32 constants for encryption algorithm values");
    return NULL;
  }

  if (napi_set_named_property(env, encryption_algorithm, "kC4EncryptionNone",
                              kC4EncryptionNone_val) != napi_ok ||
      napi_set_named_property(env, encryption_algorithm, "kC4EncryptionAES256",
                              kC4EncryptionAES256_val) != napi_ok) {
    napi_throw_error(env, NULL,
                     "Failed to set encryption algorithm constant values");
    return NULL;
  }

  return encryption_algorithm;
}