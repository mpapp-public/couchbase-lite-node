#ifndef CBNDATABASE_H
#define CBNDATABASE_H

#include <c4.h>
#include <node_api.h>

C4Database *CBNDatabaseForValue(napi_env env, napi_value value);
C4Database *CBNEnsureValidDatabaseHandle(napi_env env, napi_callback_info info,
                                         size_t expected_argc,
                                         napi_value *out_argv);

napi_value CBNOpenDatabase(napi_env env, napi_callback_info info);
napi_value CBNOpenDatabaseAgain(napi_env env, napi_callback_info ninfo);
napi_value CBNCopyDatabase(napi_env env, napi_callback_info info);
napi_value CBNGetDocumentCount(napi_env env, napi_callback_info info);
napi_value CBNCloseDatabase(napi_env env, napi_callback_info info);
napi_value CBNDeleteDatabase(napi_env env, napi_callback_info info);

napi_value CBNDeleteDatabaseAtPath(napi_env env, napi_callback_info info);
napi_value CBNShutdown(napi_env env, napi_callback_info info);
napi_value CBNGetDatabasePath(napi_env env, napi_callback_info info);
napi_value CBNGetLastSequence(napi_env env, napi_callback_info info);
napi_value CBNNextDocExpiration(napi_env env, napi_callback_info info);
napi_value CBNGetMaxRevTreeDepth(napi_env env, napi_callback_info info);
napi_value CBNSetMaxRevTreeDepth(napi_env env, napi_callback_info info);
napi_value CBNGetDatabaseUUIDs(napi_env env, napi_callback_info info);
napi_value CBNCompactDatabase(napi_env env, napi_callback_info info);

napi_value CBNBeginTransaction(napi_env env, napi_callback_info info);
napi_value CBNEndTransaction(napi_env env, napi_callback_info info);
napi_value CBNIsInTransaction(napi_env env, napi_callback_info info);

napi_value CBNGetRawDocument(napi_env env, napi_callback_info info);
napi_value CBNPutRawDocument(napi_env env, napi_callback_info info);

napi_value CNBCreateDatabaseFlags(napi_env env);

napi_value CBNCreateDocumentVersioning(napi_env env);

napi_value CBNCreateEncryptionAlgorithm(napi_env env);

napi_value CBNThrowDatabaseError(napi_env env, C4Error error, const char *msg);

#endif