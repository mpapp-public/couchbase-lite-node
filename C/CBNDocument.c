#include "CBNDocument.h"
#include "CBNDatabase.h"
#include "CBNUtilities.h"

#include <assert.h>

napi_value CBNDocumentGetRevID(napi_env env, napi_callback_info info);
napi_value CBNDocumentGetFlags(napi_env env, napi_callback_info info);
napi_value CBNDocumentGetSequence(napi_env env, napi_callback_info info);
napi_value CBNDocumentGetDocID(napi_env env, napi_callback_info info);
napi_value CBNDocumentGetSelectedRev(napi_env env, napi_callback_info info);

napi_value CBNRevisionGetRevID(napi_env env, napi_callback_info info);
napi_value CBNRevisionGetFlags(napi_env env, napi_callback_info info);
napi_value CBNRevisionGetSequence(napi_env env, napi_callback_info info);

napi_value CBNRevisionGetBody(napi_env env, napi_callback_info info);
napi_value CBNRevisionSetBody(napi_env env, napi_callback_info info);

// The incoming value can be an external that references a C4Document* or it can
// be a JS object with property _val from where you find a C4Document*
C4Document *CBNDocumentForValue(napi_env env, napi_value val) {
  C4Document *doc = NULL;
  napi_value v = val; // v = val, or external referenced by an external val._doc

  bool has_doc_property;
  napi_status doc_property_query_status;
  if ((doc_property_query_status = napi_has_named_property(
           env, val, "_doc", &has_doc_property)) == napi_ok &&
      has_doc_property) {
    if (napi_get_named_property(env, val, "_doc", &v) != napi_ok) {
      napi_throw_error(env, NULL, "Failed to get value of named property _doc");
    }
  }

  if (napi_get_value_external(env, v, (void **)&doc) != napi_ok) {
    napi_throw_error(env, NULL,
                     "Failed to unpack doc handle from external value.");
    return NULL;
  }
  return doc;
}

C4Document *CBNEnsureValidDocumentHandle(napi_env env, napi_callback_info info,
                                         size_t expected_argc,
                                         napi_value *out_argv) {
  // stack smashing can occur if C4Document APIs are called with larger numbers
  // of arguments than this -- hence allocating a large buffer here.
  napi_value argv[32];

  size_t argc = expected_argc;
  if (napi_get_cb_info(env, info, &argc, argv, NULL, NULL) != napi_ok) {
    napi_throw_error(env, NULL, "Failed to parse arguments");
    return NULL;
  }

  if (argc != expected_argc) {
    napi_throw_error(env, NULL, "Unexpected number of arguments (expected 2).");
    return NULL;
  }

  if (out_argv) {
    // FIXME: Should really copy a max of 32 napi_values, because argv above is
    // of fixed size 32 * sizeof(napi_value).
    memcpy(out_argv, argv, sizeof(napi_value) * argc);
  }

  C4Document *doc = CBNDocumentForValue(env, argv[0]);
  return doc;
}

napi_value CBNCreateDocumentFlags(napi_env env) {
  napi_value document_flags;
  if (napi_create_object(env, &document_flags) != napi_ok) {
    return NULL;
  }

  napi_value kDocDeleted_val, kDocConflicted_val, kDocHasAttachments_val,
      kDocExists_val;
  if (napi_create_int32(env, kDocDeleted, &kDocDeleted_val) != napi_ok ||
      napi_create_int32(env, kDocConflicted, &kDocConflicted_val) != napi_ok ||
      napi_create_int32(env, kDocHasAttachments, &kDocHasAttachments_val) !=
          napi_ok ||
      napi_create_int32(env, kDocExists, &kDocExists_val) != napi_ok) {
    napi_throw_error(
        env, NULL, "Failed to create int32 constants for document flag values");
    return NULL;
  }

  if (napi_set_named_property(env, document_flags, "kDocDeleted",
                              kDocDeleted_val) != napi_ok ||
      napi_set_named_property(env, document_flags, "kDocConflicted",
                              kDocConflicted_val) != napi_ok ||
      napi_set_named_property(env, document_flags, "kDocHasAttachments",
                              kDocHasAttachments_val) != napi_ok ||
      napi_set_named_property(env, document_flags, "kDocExists",
                              kDocExists_val) != napi_ok) {
    napi_throw_error(env, NULL, "Failed to set document flag values");
    return NULL;
  }

  return document_flags;
}

napi_value CBNCreateRevisionFlags(napi_env env) {
  napi_value rev_flags;
  if (napi_create_object(env, &rev_flags) != napi_ok) {
    return NULL;
  }

  napi_value kRevDeleted_val, kRevLeaf_val, kRevNew_val, kRevHasAttachments_val,
      kRevKeepBody_val, kRevIsConflict_val, kRevIsForeign_val;
  if (napi_create_int32(env, kDocDeleted, &kRevDeleted_val) != napi_ok ||
      napi_create_int32(env, kRevLeaf, &kRevLeaf_val) != napi_ok ||
      napi_create_int32(env, kRevNew, &kRevNew_val) != napi_ok ||
      napi_create_int32(env, kRevHasAttachments, &kRevHasAttachments_val) !=
          napi_ok ||
      napi_create_int32(env, kRevKeepBody, &kRevKeepBody_val) != napi_ok ||
      napi_create_int32(env, kRevIsConflict, &kRevIsConflict_val) != napi_ok ||
      napi_create_int32(env, kRevIsForeign, &kRevIsForeign_val) != napi_ok) {
    napi_throw_error(
        env, NULL, "Failed to create int32 constants for revision flag values");
    return NULL;
  }

  if (napi_set_named_property(env, rev_flags, "kDocDeleted", kRevDeleted_val) !=
          napi_ok ||
      napi_set_named_property(env, rev_flags, "kRevLeaf", kRevLeaf_val) !=
          napi_ok ||
      napi_set_named_property(env, rev_flags, "kRevNew", kRevNew_val) !=
          napi_ok ||
      napi_set_named_property(env, rev_flags, "kRevHasAttachments",
                              kRevHasAttachments_val) != napi_ok ||
      napi_set_named_property(env, rev_flags, "kRevKeepBody",
                              kRevKeepBody_val) != napi_ok ||
      napi_set_named_property(env, rev_flags, "kRevIsConflict",
                              kRevIsConflict_val) != napi_ok ||
      napi_set_named_property(env, rev_flags, "kRevIsForeign",
                              kRevIsForeign_val) != napi_ok) {
    napi_throw_error(env, NULL, "Failed to set revision flag values");
    return NULL;
  }

  return rev_flags;
}

void CBNDocumentFinalizeExternal(napi_env env, void *finalize_data,
                                 void *finalize_hint) {
  C4Document *doc = (C4Document *)finalize_data;
  c4doc_free(doc);
}

napi_value CBNDocumentValueForDocument(napi_env env, C4Document *doc) {
  napi_value ext;
  if (!CBNWrapExternal(env, doc, CBNDocumentFinalizeExternal, &ext)) {
    return NULL;
  }

  napi_value obj;
  if (napi_create_object(env, &obj) != napi_ok ||
      napi_set_named_property(env, obj, "_doc", ext) != napi_ok) {
    napi_throw_error(env, NULL,
                     "Failed to create object to wrap the external.");
    return NULL;
  };

  napi_value flags_fn, docID_fn, revID_fn, sequence_fn, selectedRev_fn;
  if (napi_create_function(env, NULL, 0, CBNDocumentGetFlags, ext, &flags_fn) !=
          napi_ok ||
      napi_create_function(env, NULL, 0, CBNDocumentGetDocID, ext, &docID_fn) !=
          napi_ok ||
      napi_create_function(env, NULL, 0, CBNDocumentGetRevID, ext, &revID_fn) !=
          napi_ok ||
      napi_create_function(env, NULL, 0, CBNDocumentGetSequence, ext,
                           &sequence_fn) != napi_ok ||
      napi_create_function(env, NULL, 0, CBNDocumentGetSelectedRev, doc,
                           &selectedRev_fn) != napi_ok) {
    napi_throw_error(env, NULL,
                     "Failed to create property functions for document");
    return NULL;
  }

  if (napi_set_named_property(env, obj, "flags", flags_fn) != napi_ok ||
      napi_set_named_property(env, obj, "docID", docID_fn) != napi_ok ||
      napi_set_named_property(env, obj, "revID", revID_fn) != napi_ok ||
      napi_set_named_property(env, obj, "sequence", sequence_fn) != napi_ok ||
      napi_set_named_property(env, obj, "selectedRev", selectedRev_fn) !=
          napi_ok) {
    napi_throw_error(env, NULL,
                     "Failed to create named properties for document external");
    return NULL;
  }

  fprintf(stderr, "Created external for document\n");

  return obj;
}

napi_value CBNDocumentGetRevID(napi_env env, napi_callback_info info) {
  napi_value value;
  if (!CBNEnsureVoidArguments(env, info, &value)) {
    return NULL;
  }

  C4Document *doc = CBNDocumentForValue(env, value);
  if (!doc) {
    return NULL;
  }

  return CBNStringValueForString(env, doc->revID);
}

napi_value CBNDocumentGetFlags(napi_env env, napi_callback_info info) {
  napi_value value;
  if (!CBNEnsureVoidArguments(env, info, &value)) {
    return false;
  }

  C4Document *doc = CBNDocumentForValue(env, value);
  if (!doc) {
    return NULL;
  }
  napi_value flags_val;
  napi_create_int32(env, doc->flags, &flags_val);
  return flags_val;
}

napi_value CBNDocumentGetSequence(napi_env env, napi_callback_info info) {
  fprintf(stderr, "Getting sequence number for document.\n");

  napi_value value;
  if (!CBNEnsureVoidArguments(env, info, &value)) {
    return false;
  }

  C4Document *doc = CBNDocumentForValue(env, value);
  if (!doc) {
    return NULL;
  }

  napi_value seq_num_val;
  napi_create_int64(env, doc->sequence, &seq_num_val);
  return seq_num_val;
}

napi_value CBNDocumentGetDocID(napi_env env, napi_callback_info info) {
  napi_value value;
  if (!CBNEnsureVoidArguments(env, info, &value)) {
    return false;
  }

  C4Document *doc = CBNDocumentForValue(env, value);
  if (!doc) {
    return NULL;
  }
  return CBNStringValueForString(env, doc->docID);
}

napi_value CBNDocumentGetSelectedRev(napi_env env, napi_callback_info info) {
  napi_value value;
  napi_value selected_rev_wrapper = NULL;

  if (!CBNEnsureVoidArguments(env, info, &value)) {
    return false;
  }

  C4Document *doc = CBNDocumentForValue(env, value);
  if (!doc) {
    return NULL;
  }

  // this gets populated lazily
  napi_value selected_rev_ext = NULL;
  bool has_selected_rev_prop;
  if ((napi_has_named_property(env, value, "_selectedRev",
                               &has_selected_rev_prop)) == napi_ok &&
      !has_selected_rev_prop) {

    // NOTE: No finalization done here, because the revision is owned by its
    // containing document (this is a sub-struct of that of the document).
    if (napi_create_external(env, &(doc->selectedRev), NULL, NULL,
                             &selected_rev_ext) != napi_ok) {
      return NULL;
    }

    if (napi_create_object(env, &selected_rev_wrapper) != napi_ok ||
        napi_set_named_property(env, selected_rev_wrapper, "_rev",
                                selected_rev_ext) != napi_ok) {
      napi_throw_error(env, NULL,
                       "Failed to create wrapper object for selected revision");
      return NULL;
    }

    // a back pointer set to the document, and from the document to the
    // revision, because both need to be unrooted for the
    // revision to be deallocated and the same external object should be
    // returned for doc's selectedRev if this is called repeatedly.
    if (napi_set_named_property(env, selected_rev_wrapper, "_doc", value) !=
            napi_ok ||
        napi_set_named_property(env, value, "_selectedRev",
                                selected_rev_wrapper)) {
      napi_throw_error(env, NULL,
                       "Failed to set named properties _doc / _selectedRev");
      return NULL;
    }

    napi_value revID_fn, flags_fn, sequence_fn, body_fn, set_body_fn;
    if (napi_create_function(env, NULL, 0, CBNRevisionGetRevID,
                             selected_rev_wrapper, &revID_fn) != napi_ok ||
        napi_create_function(env, NULL, 0, CBNRevisionGetFlags,
                             selected_rev_wrapper, &flags_fn) != napi_ok ||
        napi_create_function(env, NULL, 0, CBNRevisionGetSequence,
                             selected_rev_wrapper, &sequence_fn) != napi_ok ||
        napi_create_function(env, NULL, 0, CBNRevisionGetBody,
                             selected_rev_wrapper, &body_fn) != napi_ok ||
        napi_create_function(env, NULL, 0, CBNRevisionSetBody,
                             selected_rev_wrapper, &set_body_fn) != napi_ok) {
      napi_throw_error(env, NULL,
                       "Failing to attach revision property functions");
      return NULL;
    } else if (has_selected_rev_prop) {
      if (napi_get_named_property(env, value, "_selectedRev",
                                  &selected_rev_ext) != napi_ok) {
        napi_throw_error(env, NULL, "Failed to get value of _selectedRev");
      }
    }

    if (napi_set_named_property(env, selected_rev_wrapper, "revID", revID_fn) !=
            napi_ok ||
        napi_set_named_property(env, selected_rev_wrapper, "flags", flags_fn) !=
            napi_ok ||
        napi_set_named_property(env, selected_rev_wrapper, "sequence",
                                sequence_fn) != napi_ok ||
        napi_set_named_property(env, selected_rev_wrapper, "body", body_fn) !=
            napi_ok) {
      napi_throw_error(
          env, NULL,
          "Failed to set revID / flags / sequence / body properties");
      return NULL;
    }

    return selected_rev_wrapper;
  } else if (has_selected_rev_prop) {
    if (napi_get_named_property(env, value, "_selectedRev",
                                &selected_rev_wrapper) != napi_ok) {
      napi_throw_error(env, NULL, "Failed to get value of _selectedRev");
      return NULL;
    }
    return selected_rev_wrapper;
  }

  // TODO: Should this be an error condition?
  return NULL;
}

// argument value can either be an external that references a C4Revision*, or a
// JS object whose _rev property is the external.
C4Revision *CBNRevisionForValue(napi_env env, napi_value value) {
  napi_value v = value;

  bool has_rev_prop = false;
  if (napi_has_named_property(env, value, "_rev", &has_rev_prop) == napi_ok &&
      has_rev_prop) {
    if (napi_get_named_property(env, value, "_rev", &v) != napi_ok) {
      napi_throw_error(env, NULL, "Failed to get value of named property _rev");
      return NULL;
    }
  }

  C4Revision *rev;
  if (napi_get_value_external(env, v, (void **)&rev) != napi_ok) {
    napi_throw_error(env, NULL, "Failing to decode revision from external");
    return NULL;
  }

  return rev;
}

napi_value CBNRevisionGetRevID(napi_env env, napi_callback_info info) {
  napi_value value;
  if (!CBNEnsureVoidArguments(env, info, &value)) {
    return NULL;
  }

  C4Revision *rev = CBNRevisionForValue(env, value);
  if (!rev) {
    return NULL;
  }
  return CBNStringValueForString(env, rev->revID);
}

napi_value CBNRevisionGetFlags(napi_env env, napi_callback_info info) {
  napi_value value;
  if (!CBNEnsureVoidArguments(env, info, &value)) {
    return NULL;
  }

  C4Revision *rev = CBNRevisionForValue(env, value);
  if (!rev) {
    return NULL;
  }
  napi_value flags_val;
  napi_create_int32(env, rev->flags, &flags_val);
  return flags_val;
}

napi_value CBNRevisionGetSequence(napi_env env, napi_callback_info info) {
  napi_value value;
  if (!CBNEnsureVoidArguments(env, info, &value)) {
    return NULL;
  }

  C4Revision *rev = CBNRevisionForValue(env, value);
  if (!rev) {
    return NULL;
  }
  napi_value sequence_val;
  napi_create_int64(env, rev->sequence, &sequence_val);
  return sequence_val;
}

napi_value CBNRevisionSetBody(napi_env env, napi_callback_info info) {
  napi_value this_ref;
  napi_value value;
  if (!CBNEnsureSingleArgument(env, info, &this_ref, &value)) {
    return NULL;
  }

  C4Revision *rev = CBNRevisionForValue(env, this_ref);
  if (!rev) {
    return NULL;
  }

  C4String body = CBNStringForStringValue(env, value);
  rev->body = body;

  return NULL;
}

napi_value CBNRevisionGetBody(napi_env env, napi_callback_info info) {
  napi_value value;
  if (!CBNEnsureVoidArguments(env, info, &value)) {
    return NULL;
  }

  C4Revision *rev = CBNRevisionForValue(env, value);
  if (!rev) {
    return NULL;
  }

  return CBNStringValueForString(env, rev->body);
}

napi_value CBNGetDocument(napi_env env, napi_callback_info info) {
  napi_value args[3];
  C4Database *db = CBNEnsureValidDatabaseHandle(env, info, 3, args);
  if (!db) {
    return NULL;
  }

  C4String docID = CBNStringForStringValue(env, args[1]);
  bool mustExist = CBNValueGetBoolean(env, args[2]);

  C4Error err;
  C4Document *doc = c4doc_get(db, docID, mustExist, &err);
  if (!doc) {
    if (mustExist) {
      CBNThrowDatabaseError(env, err,
                            "Failed to recover document when it must exist.");
    }
    CBNFreeString(docID);
    return NULL;
  }
  CBNFreeString(docID);

  return CBNDocumentValueForDocument(env, doc);
}

napi_value CBNGetDocumentBySequence(napi_env env, napi_callback_info info) {
  napi_value args[2];

  C4Database *db = CBNEnsureValidDatabaseHandle(env, info, 2, args);
  if (!db) {
    return NULL;
  }

  uint32_t seq_num;
  if (napi_get_value_uint32(env, args[1], &seq_num) != napi_ok) {
    napi_throw_error(env, NULL,
                     "Failed to decode sequence number from 2nd argument");
    return NULL;
  }

  C4Error err;
  C4Document *doc = c4doc_getBySequence(db, seq_num, &err);
  if (!doc) {
    return NULL;
  }

  return CBNDocumentValueForDocument(env, doc);
}

napi_value CBNSaveDocument(napi_env env, napi_callback_info info) {
  napi_value args[2];
  C4Document *doc = CBNEnsureValidDocumentHandle(env, info, 2, args);
  if (!doc) {
    return CBNBoolean(env, false);
  }

  uint32_t max_rev_tree_depth;
  if (napi_get_value_uint32(env, args[1], &max_rev_tree_depth)) {
    napi_throw_error(env, NULL,
                     "Failed to decode max rev tree depth from 2nd argument");
    return CBNBoolean(env, false);
  }

  C4Error err;
  if (!c4doc_save(doc, max_rev_tree_depth, &err)) {
    CBNThrowDatabaseError(env, err, "Failed to save document");
    return CBNBoolean(env, false);
  }

  return CBNBoolean(env, true);
}

napi_value CBNSelectDocumentRevision(napi_env env, napi_callback_info info) {
  napi_value args[3];
  C4Document *doc = CBNEnsureValidDocumentHandle(env, info, 3, args);

  if (!doc) {
    return CBNBoolean(env, false);
  }

  C4String revID = CBNStringForStringValue(env, args[1]);
  bool with_body = CBNValueGetBoolean(env, args[2]);

  C4Error err;
  bool success = false;
  if (!(success = c4doc_selectRevision(doc, revID, with_body, &err))) {
    CBNThrowDatabaseError(env, err, "Failed to select revision");
  }
  CBNFreeString(revID);
  return CBNBoolean(env, success);
}

napi_value CBNSelectCurrentRevision(napi_env env, napi_callback_info info) {
  C4Document *doc = CBNEnsureValidDocumentHandle(env, info, 1, NULL);
  if (!doc) {
    return NULL;
  }
  return CBNBoolean(env, c4doc_selectCurrentRevision(doc));
}

napi_value CBNLoadRevisionBody(napi_env env, napi_callback_info info) {
  C4Document *doc = CBNEnsureValidDocumentHandle(env, info, 1, NULL);
  if (!doc) {
    return CBNBoolean(env, false);
  }

  C4Error err;
  if (!c4doc_loadRevisionBody(doc, &err)) {
    CBNThrowDatabaseError(env, err, "Failed to load revision body");
    return CBNBoolean(env, false);
  }

  return CBNBoolean(env, true);
}

napi_value CBNDetachRevisionBody(napi_env env, napi_callback_info info) {
  C4Document *doc = CBNEnsureValidDocumentHandle(env, info, 1, NULL);
  if (!doc) {
    return CBNBoolean(env, false);
  }

  C4String str = c4doc_detachRevisionBody(doc);
  return CBNStringValueForString(env, str);
}

napi_value CBNHasRevisionBody(napi_env env, napi_callback_info info) {
  C4Document *doc = CBNEnsureValidDocumentHandle(env, info, 1, NULL);
  if (!doc) {
    return CBNBoolean(env, false);
  }
  return CBNBoolean(env, c4doc_hasRevisionBody(doc));
}

napi_value CBNSelectParentRevision(napi_env env, napi_callback_info info) {
  C4Document *doc = CBNEnsureValidDocumentHandle(env, info, 1, NULL);
  if (!doc) {
    return CBNBoolean(env, false);
  }
  return CBNBoolean(env, c4doc_selectParentRevision(doc));
}

napi_value CBNSelectNextRevision(napi_env env, napi_callback_info info) {
  C4Document *doc = CBNEnsureValidDocumentHandle(env, info, 1, NULL);
  if (!doc) {
    return CBNBoolean(env, false);
  }
  return CBNBoolean(env, c4doc_selectNextRevision(doc));
}

napi_value CBNSelectNextLeafRevision(napi_env env, napi_callback_info info) {
  napi_value args[3];
  C4Document *doc = CBNEnsureValidDocumentHandle(env, info, 1, args);
  if (!doc) {
    return CBNBoolean(env, false);
  }

  bool include_deleted, with_body;
  if (napi_get_value_bool(env, args[1], &include_deleted) != napi_ok ||
      napi_get_value_bool(env, args[2], &with_body)) {
    return CBNBoolean(env, false);
  }

  C4Error err;
  if (!c4doc_selectNextLeafRevision(doc, include_deleted, with_body, &err)) {
    CBNThrowDatabaseError(env, err, "Failed to select next leaf revision");
    return CBNBoolean(env, false);
  }

  return CBNBoolean(env, true);
}

napi_value CBNSelectFirstPossibleAncestorOf(napi_env env,
                                            napi_callback_info info) {
  napi_value args[2];
  C4Document *doc = CBNEnsureValidDocumentHandle(env, info, 2, args);
  C4String rev_id = CBNStringForStringValue(env, args[1]);

  bool success = c4doc_selectFirstPossibleAncestorOf(doc, rev_id);
  CBNFreeString(rev_id);
  return CBNBoolean(env, success);
}

napi_value CBNSelectNextPossibleAncestorOf(napi_env env,
                                           napi_callback_info info) {
  napi_value args[2];
  C4Document *doc = CBNEnsureValidDocumentHandle(env, info, 2, args);
  C4String rev_id = CBNStringForStringValue(env, args[1]);

  bool success = c4doc_selectNextPossibleAncestorOf(doc, rev_id);
  CBNFreeString(rev_id);
  return CBNBoolean(env, success);
}

napi_value CBNSelectCommonAncestorRevision(napi_env env,
                                           napi_callback_info info) {
  napi_value args[3];
  C4Document *doc = CBNEnsureValidDocumentHandle(env, info, 3, args);

  C4String rev1_id = CBNStringForStringValue(env, args[1]);
  C4String rev2_id = CBNStringForStringValue(env, args[2]);

  bool success = c4doc_selectCommonAncestorRevision(doc, rev1_id, rev2_id);
  CBNFreeString(rev1_id);
  CBNFreeString(rev2_id);
  return CBNBoolean(env, success);
}

napi_value CBNRevGetGeneration(napi_env env, napi_callback_info info) {
  napi_value argv[1];
  size_t argc = 1;
  if (napi_get_cb_info(env, info, &argc, argv, NULL, NULL) != napi_ok) {
    napi_throw_error(env, NULL, "Failed to parse arguments");
    return NULL;
  }

  if (argc != 1) {
    napi_throw_error(env, NULL, "Unexpected number of arguments (expected 2).");
    return NULL;
  }

  C4String rev_id = CBNStringForStringValue(env, argv[0]);
  uint32_t gen = c4rev_getGeneration(rev_id);
  CBNFreeString(rev_id);

  napi_value gen_val;
  if (napi_create_uint32(env, gen, &gen_val) != napi_ok) {
    napi_throw_error(env, NULL,
                     "Failed to wrap generation to a 32-bit unsigned value");
    return NULL;
  }

  return gen_val;
}

napi_value CBNRemoveRevisionBody(napi_env env, napi_callback_info info) {
  C4Document *doc = CBNEnsureValidDocumentHandle(env, info, 1, NULL);
  return CBNBoolean(env, c4doc_removeRevisionBody(doc));
}

napi_value CBNPurgeRevision(napi_env env, napi_callback_info info) {
  napi_value args[2];
  C4Document *doc = CBNEnsureValidDocumentHandle(env, info, 2, args);
  if (!doc) {
    return CBNBoolean(env, false);
  }

  C4String rev_id = CBNStringForStringValue(env, args[1]);

  C4Error err;
  bool success = false;
  if (!(success = c4doc_purgeRevision(doc, rev_id, &err))) {
    CBNThrowDatabaseError(env, err, "Failed to purge revision");
  }
  CBNFreeString(rev_id);

  return CBNBoolean(env, success);
}

napi_value CBNResolveConflict(napi_env env, napi_callback_info info) {
  napi_value args[4];
  C4Document *doc = CBNEnsureValidDocumentHandle(env, info, 4, args);
  if (!doc) {
    return CBNBoolean(env, false);
  }

  C4String winning_rev_id = {0, 0};
  C4String losing_rev_id = {0, 0};
  C4Slice merged_body = {0, 0};
  C4Error err;

  if (!c4doc_resolveConflict(doc, winning_rev_id, losing_rev_id, merged_body,
                             &err)) {
    CBNThrowDatabaseError(env, err, "Failed to resolve conflict");
    return CBNBoolean(env, false);
  }

  return CBNBoolean(env, true);
}
