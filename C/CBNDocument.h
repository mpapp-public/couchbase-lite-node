#ifndef CBNDOCUMENT_H
#define CBNDOCUMENT_H

#include <c4.h>
#include <node_api.h>

napi_value CBNCreateDocumentFlags(napi_env env);
napi_value CBNCreateRevisionFlags(napi_env env);

C4Document *CBNDocumentForValue(napi_env env, napi_value value);

napi_value CBNGetDocument(napi_env env, napi_callback_info info);
napi_value CBNGetDocumentBySequence(napi_env env, napi_callback_info info);
napi_value CBNSaveDocument(napi_env env, napi_callback_info info);

// Revisions

napi_value CBNSelectDocumentRevision(napi_env env, napi_callback_info info);
napi_value CBNSelectCurrentRevision(napi_env env, napi_callback_info info);
napi_value CBNLoadRevisionBody(napi_env env, napi_callback_info info);
napi_value CBNDetachRevisionBody(napi_env env, napi_callback_info info);
napi_value CBNHasRevisionBody(napi_env env, napi_callback_info info);
napi_value CBNSelectParentRevision(napi_env env, napi_callback_info info);
napi_value CBNSelectNextRevision(napi_env env, napi_callback_info info);
napi_value CBNSelectNextLeafRevision(napi_env env, napi_callback_info info);
napi_value CBNSelectFirstPossibleAncestorOf(napi_env env,
                                            napi_callback_info info);
napi_value CBNSelectNextPossibleAncestorOf(napi_env env,
                                           napi_callback_info info);

napi_value CBNSelectCommonAncestorRevision(napi_env env,
                                           napi_callback_info info);
napi_value CBNRevGetGeneration(napi_env env, napi_callback_info info);
napi_value CBNRemoveRevisionBody(napi_env env, napi_callback_info info);
napi_value CBNPurgeRevision(napi_env env, napi_callback_info info);

// Conflicts

napi_value CBNResolveConflict(napi_env env, napi_callback_info info);

#endif