#include <assert.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>

#include <node_api.h>

#include <c4.h>
#include <c4Base.h>

#include "CBNBase.h"
#include "CBNDatabase.h"
#include "CBNDocument.h"

napi_value Init(napi_env env, napi_value exports) {
  napi_value c4db_open_fn, c4db_open_again_fn, c4db_copy_database_fn,
      c4db_getDocumentCount_fn, c4db_close_fn, c4db_delete_fn,
      c4db_delete_at_path_fn, c4_shutdown_fn, c4db_getPath_fn,
      c4db_getLastSequence_fn, c4db_nextDocExpiration_fn,
      c4db_getMaxRevTreeDepth_fn, c4db_setMaxRevTreeDepth_fn, c4db_getUUIDs_fn,
      c4db_beginTransaction_fn, c4db_endTransaction_fn, c4db_isInTransaction_fn,
      c4db_compact_fn, c4raw_get_fn, c4raw_put_fn, c4_getBuildInfo_fn,
      c4_getObjectCount_fn, c4_dumpInstances_fn, c4log_getDomainName_fn,
      c4log_getDomain_fn, c4log_getLevel_fn, c4log_setLevel_fn;
  ;
  if ((napi_create_function(env, NULL, 0, CBNOpenDatabase, NULL,
                            &c4db_open_fn) != napi_ok) ||
      napi_create_function(env, NULL, 0, CBNOpenDatabaseAgain, NULL,
                           &c4db_open_again_fn) != napi_ok ||
      napi_create_function(env, NULL, 0, CBNCopyDatabase, NULL,
                           &c4db_copy_database_fn) != napi_ok ||
      napi_create_function(env, NULL, 0, CBNGetDocumentCount, NULL,
                           &c4db_getDocumentCount_fn) != napi_ok ||
      napi_create_function(env, NULL, 0, CBNCloseDatabase, NULL,
                           &c4db_close_fn) != napi_ok ||
      napi_create_function(env, NULL, 0, CBNDeleteDatabase, NULL,
                           &c4db_delete_fn) != napi_ok ||
      napi_create_function(env, NULL, 0, CBNDeleteDatabaseAtPath, NULL,
                           &c4db_delete_at_path_fn) != napi_ok ||
      napi_create_function(env, NULL, 0, CBNShutdown, NULL, &c4_shutdown_fn) !=
          napi_ok ||
      napi_create_function(env, NULL, 0, CBNGetDatabasePath, NULL,
                           &c4db_getPath_fn) != napi_ok ||
      napi_create_function(env, NULL, 0, CBNGetLastSequence, NULL,
                           &c4db_getLastSequence_fn) != napi_ok ||
      napi_create_function(env, NULL, 0, CBNNextDocExpiration, NULL,
                           &c4db_nextDocExpiration_fn) != napi_ok ||
      napi_create_function(env, NULL, 0, CBNGetMaxRevTreeDepth, NULL,
                           &c4db_getMaxRevTreeDepth_fn) != napi_ok ||
      napi_create_function(env, NULL, 0, CBNSetMaxRevTreeDepth, NULL,
                           &c4db_setMaxRevTreeDepth_fn) != napi_ok ||
      napi_create_function(env, NULL, 0, CBNGetDatabaseUUIDs, NULL,
                           &c4db_getUUIDs_fn) != napi_ok ||
      napi_create_function(env, NULL, 0, CBNBeginTransaction, NULL,
                           &c4db_beginTransaction_fn) != napi_ok ||
      napi_create_function(env, NULL, 0, CBNEndTransaction, NULL,
                           &c4db_endTransaction_fn) != napi_ok ||
      napi_create_function(env, NULL, 0, CBNIsInTransaction, NULL,
                           &c4db_isInTransaction_fn) != napi_ok ||
      napi_create_function(env, NULL, 0, CBNCompactDatabase, NULL,
                           &c4db_compact_fn) != napi_ok ||
      napi_create_function(env, NULL, 0, CBNGetRawDocument, NULL,
                           &c4raw_get_fn) != napi_ok ||
      napi_create_function(env, NULL, 0, CBNPutRawDocument, NULL,
                           &c4raw_put_fn) != napi_ok ||
      napi_create_function(env, NULL, 0, CBNGetBuildInfo, NULL,
                           &c4_getBuildInfo_fn) != napi_ok ||
      napi_create_function(env, NULL, 0, CBNGetObjectCount, NULL,
                           &c4_getObjectCount_fn) != napi_ok ||
      napi_create_function(env, NULL, 0, CBNDumpInstances, NULL,
                           &c4_dumpInstances_fn) != napi_ok ||
      napi_create_function(env, NULL, 0, CBNLogGetDomainName, NULL,
                           &c4log_getDomainName_fn) != napi_ok ||
      napi_create_function(env, NULL, 0, CBNLogGetDomain, NULL,
                           &c4log_getDomain_fn) != napi_ok ||
      napi_create_function(env, NULL, 0, CBNLogGetLevel, NULL,
                           &c4log_getLevel_fn) != napi_ok ||
      napi_create_function(env, NULL, 0, CBNLogSetLevel, NULL,
                           &c4log_setLevel_fn) != napi_ok) {
    napi_throw_error(env, NULL, "Unable to create a JS function corresponding "
                                "to one or more of the CBN function.");
    return NULL;
  }

  napi_value c4doc_get_fn, c4doc_getBySequence_fn, c4doc_save_fn,
      c4doc_selectRevision_fn, c4doc_selectCurrentRevision_fn,
      c4doc_loadRevisionBody_fn, c4doc_detachRevisionBody_fn,
      c4doc_hasRevisionBody_fn, c4doc_selectParentRevision_fn,
      c4doc_selectNextRevision_fn, c4doc_selectNextLeafRevision_fn,
      c4doc_selectFirstPossibleAncestorOf_fn,
      c4doc_selectNextPossibleAncestorOf_fn,
      c4doc_selectCommonAncestorRevision_fn, c4rev_getGeneration_fn,
      c4doc_removeRevisionBody_fn, c4doc_purgeRevision_fn,
      c4doc_resolveConflict_fn;

  if (napi_create_function(env, NULL, 0, CBNGetDocument, NULL, &c4doc_get_fn) !=
          napi_ok ||
      napi_create_function(env, NULL, 0, CBNGetDocumentBySequence, NULL,
                           &c4doc_getBySequence_fn) != napi_ok ||
      napi_create_function(env, NULL, 0, CBNSaveDocument, NULL,
                           &c4doc_save_fn) != napi_ok ||
      napi_create_function(env, NULL, 0, CBNSelectDocumentRevision, NULL,
                           &c4doc_selectRevision_fn) != napi_ok ||
      napi_create_function(env, NULL, 0, CBNSelectCurrentRevision, NULL,
                           &c4doc_selectCurrentRevision_fn) != napi_ok ||
      napi_create_function(env, NULL, 0, CBNLoadRevisionBody, NULL,
                           &c4doc_loadRevisionBody_fn) != napi_ok ||
      napi_create_function(env, NULL, 0, CBNDetachRevisionBody, NULL,
                           &c4doc_detachRevisionBody_fn) != napi_ok ||
      napi_create_function(env, NULL, 0, CBNHasRevisionBody, NULL,
                           &c4doc_hasRevisionBody_fn) != napi_ok ||
      napi_create_function(env, NULL, 0, CBNSelectParentRevision, NULL,
                           &c4doc_selectParentRevision_fn) != napi_ok ||
      napi_create_function(env, NULL, 0, CBNSelectNextRevision, NULL,
                           &c4doc_selectNextRevision_fn) != napi_ok ||
      napi_create_function(env, NULL, 0, CBNSelectNextLeafRevision, NULL,
                           &c4doc_selectNextLeafRevision_fn) != napi_ok ||
      napi_create_function(env, NULL, 0, CBNSelectFirstPossibleAncestorOf, NULL,
                           &c4doc_selectFirstPossibleAncestorOf_fn) !=
          napi_ok ||
      napi_create_function(env, NULL, 0, CBNSelectNextPossibleAncestorOf, NULL,
                           &c4doc_selectNextPossibleAncestorOf_fn) != napi_ok ||
      napi_create_function(env, NULL, 0, CBNSelectCommonAncestorRevision, NULL,
                           &c4doc_selectCommonAncestorRevision_fn) != napi_ok ||
      napi_create_function(env, NULL, 0, CBNRevGetGeneration, NULL,
                           &c4rev_getGeneration_fn) != napi_ok ||
      napi_create_function(env, NULL, 0, CBNRemoveRevisionBody, NULL,
                           &c4doc_removeRevisionBody_fn) != napi_ok ||
      napi_create_function(env, NULL, 0, CBNPurgeRevision, NULL,
                           &c4doc_purgeRevision_fn) != napi_ok ||
      napi_create_function(env, NULL, 0, CBNResolveConflict, NULL,
                           &c4doc_resolveConflict_fn) != napi_ok) {
  }

  napi_set_named_property(env, exports, "c4db_open", c4db_open_fn);
  napi_set_named_property(env, exports, "c4db_openAgain", c4db_open_again_fn);
  napi_set_named_property(env, exports, "c4db_copy", c4db_copy_database_fn);
  napi_set_named_property(env, exports, "c4db_getDocumentCount",
                          c4db_getDocumentCount_fn);
  napi_set_named_property(env, exports, "c4db_close", c4db_close_fn);
  napi_set_named_property(env, exports, "c4db_delete", c4db_delete_fn);
  napi_set_named_property(env, exports, "c4db_deleteAtPath",
                          c4db_delete_at_path_fn);
  napi_set_named_property(env, exports, "c4_shutdown", c4_shutdown_fn);

  napi_set_named_property(env, exports, "c4db_getPath", c4db_getPath_fn);
  napi_set_named_property(env, exports, "c4db_getLastSequence",
                          c4db_getLastSequence_fn);

  napi_set_named_property(env, exports, "c4db_nextDocExpiration",
                          c4db_nextDocExpiration_fn);
  napi_set_named_property(env, exports, "c4db_getMaxRevTreeDepth",
                          c4db_getMaxRevTreeDepth_fn);
  napi_set_named_property(env, exports, "c4db_setMaxRevTreeDepth",
                          c4db_setMaxRevTreeDepth_fn);

  napi_set_named_property(env, exports, "c4db_getUUIDs", c4db_getUUIDs_fn);
  napi_set_named_property(env, exports, "c4db_compact", c4db_compact_fn);

  napi_set_named_property(env, exports, "c4db_beginTransaction",
                          c4db_beginTransaction_fn);
  napi_set_named_property(env, exports, "c4db_endTransaction",
                          c4db_endTransaction_fn);
  napi_set_named_property(env, exports, "c4db_isInTransaction",
                          c4db_isInTransaction_fn);

  napi_value kC4LocalDocStore_val, kC4InfoStore_val;
  napi_create_string_utf8(env, kC4LocalDocStore.buf, kC4LocalDocStore.size,
                          &kC4LocalDocStore_val);
  napi_create_string_utf8(env, kC4InfoStore.buf, kC4InfoStore.size,
                          &kC4InfoStore_val);
  napi_set_named_property(env, exports, "kC4LocalDocStore",
                          kC4LocalDocStore_val);
  napi_set_named_property(env, exports, "kC4InfoStore", kC4InfoStore_val);

  napi_set_named_property(env, exports, "c4raw_get", c4raw_get_fn);
  napi_set_named_property(env, exports, "c4raw_put", c4raw_put_fn);

  napi_set_named_property(env, exports, "c4_getBuildInfo", c4_getBuildInfo_fn);
  napi_set_named_property(env, exports, "c4_getObjectCount",
                          c4_getObjectCount_fn);
  napi_set_named_property(env, exports, "c4_dumpInstances",
                          c4_dumpInstances_fn);

  napi_set_named_property(env, exports, "C4LogDomain",
                          CBNCreateDefaultLogDomains(env));

  napi_set_named_property(env, exports, "C4LogLevel", CBNCreateLogLevels(env));

  napi_set_named_property(env, exports, "C4DatabaseFlags",
                          CNBCreateDatabaseFlags(env));

  napi_value kC4SQLiteStorageEngine_val;
  if (napi_create_string_utf8(env, kC4SQLiteStorageEngine,
                              strlen(kC4SQLiteStorageEngine),
                              &kC4SQLiteStorageEngine_val)) {
    napi_throw_error(env, NULL, "Creating "
                                "'kC4SQLiteStorageEngi"
                                "ne' constant failed");
    return NULL;
  }

  napi_set_named_property(env, exports, "kC4SQLiteStorageEngine",
                          kC4SQLiteStorageEngine_val);

  napi_set_named_property(env, exports, "C4DocumentVersioning",
                          CBNCreateDocumentVersioning(env));

  napi_set_named_property(env, exports, "C4EncryptionAlgorithm",
                          CBNCreateEncryptionAlgorithm(env));

  napi_set_named_property(env, exports, "c4log_getDomainName",
                          c4log_getDomainName_fn);
  napi_set_named_property(env, exports, "c4log_getDomain", c4log_getDomain_fn);
  napi_set_named_property(env, exports, "c4log_getLevel", c4log_getLevel_fn);
  napi_set_named_property(env, exports, "c4log_setLevel", c4log_setLevel_fn);

  napi_set_named_property(env, exports, "C4DocumentFlags",
                          CBNCreateDocumentFlags(env));
  napi_set_named_property(env, exports, "C4RevisionFlags",
                          CBNCreateRevisionFlags(env));

  // Document APIs

  napi_set_named_property(env, exports, "c4doc_get", c4doc_get_fn);
  napi_set_named_property(env, exports, "c4doc_getBySequence",
                          c4doc_getBySequence_fn);
  napi_set_named_property(env, exports, "c4doc_save", c4doc_save_fn);

  // Revisions

  napi_set_named_property(env, exports, "c4doc_selectRevision",
                          c4doc_selectRevision_fn);
  napi_set_named_property(env, exports, "c4doc_selectCurrentRevision",
                          c4doc_selectCurrentRevision_fn);
  napi_set_named_property(env, exports, "c4doc_loadRevisionBody",
                          c4doc_loadRevisionBody_fn);
  napi_set_named_property(env, exports, "c4doc_detachRevisionBody",
                          c4doc_detachRevisionBody_fn);
  napi_set_named_property(env, exports, "c4doc_hasRevisionBody",
                          c4doc_hasRevisionBody_fn);
  napi_set_named_property(env, exports, "c4doc_selectParentRevision",
                          c4doc_selectParentRevision_fn);
  napi_set_named_property(env, exports, "c4doc_selectNextRevision",
                          c4doc_selectNextRevision_fn);
  napi_set_named_property(env, exports, "c4doc_selectNextLeafRevision",
                          c4doc_selectNextLeafRevision_fn);
  napi_set_named_property(env, exports, "c4doc_selectFirstPossibleAncestorOf",
                          c4doc_selectFirstPossibleAncestorOf_fn);
  napi_set_named_property(env, exports, "c4doc_selectNextPossibleAncestorOf",
                          c4doc_selectNextPossibleAncestorOf_fn);
  napi_set_named_property(env, exports, "c4doc_selectCommonAncestorRevision",
                          c4doc_selectCommonAncestorRevision_fn);
  napi_set_named_property(env, exports, "c4rev_getGeneration",
                          c4rev_getGeneration_fn);
  napi_set_named_property(env, exports, "c4doc_removeRevisionBody",
                          c4doc_removeRevisionBody_fn);
  napi_set_named_property(env, exports, "c4doc_purgeRevision",
                          c4doc_purgeRevision_fn);

  // Conflicts
  napi_set_named_property(env, exports, "c4doc_resolveConflict",
                          c4doc_resolveConflict_fn);

  return exports;
}

NAPI_MODULE(NODE_GYP_MODULE_NAME, Init)