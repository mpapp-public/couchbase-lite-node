#include "CBNUtilities.h"

#include <c4.h>

napi_value CBNBoolean(napi_env env, bool truth) {
  napi_value val;
  if (napi_create_int32(env, truth, &val) != napi_ok) {
    napi_throw_error(
        env, NULL,
        "Failing to create an integral value as a basis for a boolean.");
    return false;
  }

  napi_value bool_val;
  if (napi_coerce_to_bool(env, val, &bool_val) != napi_ok) {
    napi_throw_error(
        env, NULL,
        "Failing to create an integral value as a basis for a boolean.");
    return false;
  }

  return bool_val;
}

bool CBNValueGetBoolean(napi_env env, napi_value val) {
  bool b;
  if (napi_get_value_bool(env, val, &b) != napi_ok) {
    napi_throw_error(env, NULL, "Failing to decode boolean from value.");
    return false;
  }
  return b;
}

char *CBNCStringForStringValue(napi_env env, napi_value value) {
  char str[2048];
  size_t result_size = 0;

  if (napi_get_value_string_utf8(env, value, str, 2048, &result_size) !=
      napi_ok) {
    napi_throw_error(env, NULL, "Failed to decode string from value.");
    return NULL;
  }

  // if our buffer was not big enough, then let's take the heavy guns out.
  if (result_size > 2048) {
    char *dyn_str = malloc((result_size + 1) * sizeof(char));
    int dyn_str_length = result_size;
    if (napi_get_value_string_utf8(env, value, dyn_str, dyn_str_length,
                                   &result_size) != napi_ok) {
      napi_throw_error(
          env, NULL,
          "Failed to decode into dynamically allocated string fom value.");
      return NULL;
    }
    return dyn_str;
  }
  char *out_str = malloc((result_size + 1) * sizeof(char));
  strncpy(out_str, str, result_size + 1);

  return out_str;
}

napi_value CBNStringValueForString(napi_env env, C4String string) {
  if (!string.buf && string.size == 0) {
    return NULL;
  }

  napi_value str;
  if (napi_create_string_utf8(env, string.buf, string.size, &str) != napi_ok) {
    napi_throw_error(env, NULL,
                     "Failed to create string value for incoming slice.");
    return NULL;
  }

  return str;
}

void CBNFreeString(C4String str) { free((void *)(str.buf)); }

bool CBNEnsureVoidArguments(napi_env env, napi_callback_info info,
                            napi_value *this_ref) {
  size_t expected_argc = 0;
  napi_value argv[32]; // at least with current N-API implementation, stack
                       // smashing can occur if this value is too small.

  size_t argc = expected_argc;
  if (napi_get_cb_info(env, info, &argc, argv, this_ref, NULL) != napi_ok) {
    napi_throw_error(env, NULL, "Failed to parse arguments");
    return false;
  }

  if (argc != expected_argc) {
    napi_throw_error(env, NULL, "Unexpected number of arguments (expected 2).");
    return false;
  }

  return true;
}

bool CBNEnsureSingleArgument(napi_env env, napi_callback_info info,
                             napi_value *this_ref, napi_value *out_value) {
  napi_value argv[1];
  size_t argc = 1;
  if (napi_get_cb_info(env, info, &argc, argv, this_ref, NULL) != napi_ok) {
    napi_throw_error(env, NULL, "Failed to parse arguments");
    return false;
  }

  if (argc != 1) {
    napi_throw_error(env, NULL, "Unexpected number of arguments (expected 1).");
    return false;
  }

  if (out_value) {
    memcpy(out_value, argv, sizeof(napi_value) * argc);
  }

  return true;
}

bool CBNWrapExternal(napi_env env, void *external_p, napi_finalize finalizer,
                     napi_value *result) {
  napi_value ext;

  if (napi_create_external(env, external_p, finalizer, NULL, &ext) != napi_ok) {
    napi_throw_error(env, NULL, "Failed to create external for a document");

    if (result) {
      *result = NULL;
    }

    return false;
  }

  if (result) {
    *result = ext;
  }

  return true;
}