#include <c4.h>
#include <node_api.h>

#ifndef CBNUTILITIES_H
#define CBNUTILITIES_H

napi_value CBNBoolean(napi_env env, bool truth);
bool CBNValueGetBoolean(napi_env env, napi_value val);

napi_value CBNStringValueForString(napi_env env, C4String string);

char *CBNCStringForStringValue(napi_env env, napi_value value);

static inline C4String CBNStringForStringValue(napi_env env, napi_value value) {
  char *out_str = CBNCStringForStringValue(env, value);
  C4String str;
  str.size = strlen(out_str);
  str.buf = out_str;
  return str;
}

// Use this *only* when you used CBNStringForStringValue to allocate (uses
// malloc for the buffer, not a C++ String like c4str).
void CBNFreeString(C4String str);

bool CBNEnsureVoidArguments(napi_env env, napi_callback_info info,
                            napi_value *thisRef);

bool CBNEnsureSingleArgument(napi_env env, napi_callback_info info,
                             napi_value *this_ref, napi_value *out_value);

bool CBNWrapExternal(napi_env env, void *external_p, napi_finalize finalizer,
                     napi_value *result);

#endif // CBNUTILITIES_H