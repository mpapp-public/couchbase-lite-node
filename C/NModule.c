#include <assert.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>

#include <node_api.h>

napi_value Init(napi_env env, napi_value exports) {
  /*
  napi_value c4db_open_fn;

  if ((napi_create_function(env, NULL, 0, CBNOpenDatabase, NULL,
                            &c4db_open_fn) != napi_ok) {
    napi_throw_error(env, NULL, "Unable to create a JS function corresponding "
                                "to one or more of the Fleece functions.");
    return NULL;
  }

  napi_set_named_property(env, exports, "c4db_open", c4db_open_fn);
  */

  return exports;
}

NAPI_MODULE(NODE_GYP_MODULE_NAME, Init)