export as namespace Fleece;

/** Types of Fleece values. Basically JSON, with the addition of Data (raw blob). */
declare enum FLValueType {
  kFLUndefined = -1, // Type of a nullptr FLValue (i.e. no such value)
  kFLNull = 0,
  kFLBoolean,
  kFLNumber,
  kFLString,
  kFLData,
  kFLArray,
  kFLDict
}

export interface Fleece {}
