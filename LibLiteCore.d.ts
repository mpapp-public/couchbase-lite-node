export as namespace LiteCore;

export type DatabaseFlagValue = number;

export interface DatabaseFlags {
  kC4DB_Create: DatabaseFlagValue;
  kC4DB_ReadOnly: DatabaseFlagValue;
  kC4DB_AutoCompact: DatabaseFlagValue;
  kC4DB_SharedKeys: DatabaseFlagValue;
  kC4DB_NoUpgrade: DatabaseFlagValue;
  kC4DB_NonObservable: DatabaseFlagValue;
}

export type LogDomainValue = number;

export interface LogDomain {
  kC4DefaultLog: LogDomainValue;
  kC4DatabaseLog: LogDomainValue;
  kC4QueryLog: LogDomainValue;
  kC4SyncLog: LogDomainValue;
  kC4WebSocketLog: LogDomainValue;
}

export type LogLevelValue = number;

export interface LogLevel {
  kC4LogDebug: LogLevelValue;
  kC4LogVerbose: LogLevelValue;
  kC4LogInfo: LogLevelValue;
  kC4LogWarning: LogLevelValue;
  kC4LogError: LogLevelValue;
  kC4LogNone: LogLevelValue;
}

export type DocumentVersioningValue = number;

export interface DocumentVersioning {
  kC4RevisionTrees: DocumentVersioningValue;
  kC4VersionVectors: DocumentVersioningValue;
}

export interface StorageEngine {}

export type EncryptionAlgorithmValue = number;

export interface EncryptionAlgorithm {
  kC4EncryptionNone: EncryptionAlgorithmValue;
  kC4EncryptionAES256: EncryptionAlgorithmValue;
}

export interface EncryptionKey {
  algorithm: EncryptionAlgorithmValue;
  bytes: Buffer;
}

export interface DatabaseConfiguration {
  flags: DatabaseFlagValue;
  storageEngine: StorageEngine;
  versioning: DocumentVersioningValue;
  encryptionKey: EncryptionKey;
}

export interface UUID {
  bytes: Buffer;
}

export interface UUIDs {
  privateUUID: UUID;
  publicUUID: UUID;
}

export interface RawDocument {
  key(): string;
  meta(): string;
  body(): string;
}

export interface Database {}

export type DocumentFlagsValue = number;
export type RevisionFlagsValue = number;

export interface RevisionFlags {
  kRevDeleted: RevisionFlagsValue;
  kRevLeaf: RevisionFlagsValue;
  kRevNew: RevisionFlagsValue;
  kRevHasAttachments: RevisionFlagsValue;
  kRevKeepBody: RevisionFlagsValue;
  kRevIsConflict: RevisionFlagsValue;
  kRevIsForeign: RevisionFlagsValue;
}

export interface Revision {
  revID(): string;
  flags(): RevisionFlagsValue;
  sequence(): number;
  body(): string;
}

export interface DocumentFlags {
  kDocDeleted: DocumentFlagsValue;
  kDocConflicted: DocumentFlagsValue;
  kDocHasAttachments: DocumentFlagsValue;
  kDocExists: DocumentFlagsValue;
}

export interface Document {
  flags(): DocumentFlagsValue;
  docID(): string;
  revID(): string;
  sequence(): number;
  selectedRev(): Revision;
}

export interface C4 {
  C4DatabaseFlags: DatabaseFlags;
  C4LogDomain: LogDomain;
  C4LogLevel: LogLevel;
  C4DocumentVersioning: DocumentVersioning;
  C4EncryptionAlgorithm: EncryptionAlgorithm;

  kC4SQLiteStorageEngine: string;

  c4_getBuildInfo(): string;

  c4log_getLevel(domain: LogDomainValue): LogLevelValue;
  c4log_setLevel(domain: LogDomainValue, level: LogLevelValue): void;
  c4log_getDomain(name: string, mustExist: boolean): LogDomainValue;
  c4log_getDomainName(domain: LogDomainValue): string;

  c4db_open(path: string, config: DatabaseConfiguration): Database;
  c4db_openAgain(db: Database): boolean;
  c4db_close(db: Database): boolean;

  c4db_getDocumentCount(db: Database): number;

  c4db_delete(db: Database): boolean;
  c4db_deleteAtPath(dbPath: string): boolean;
  c4_shutdown(): boolean;

  c4db_getPath(db: Database): string;

  c4db_getLastSequence(db: Database): number;
  c4db_nextDocExpiration(db: Database): number;

  c4db_getMaxRevTreeDepth(db: Database): number;
  c4db_setMaxRevTreeDepth(db: Database, val: number): boolean;

  c4db_getUUIDs(db: Database): UUIDs;
  c4db_compact(db: Database): boolean;

  c4db_isInTransaction(db: Database): boolean;
  c4db_beginTransaction(db: Database): boolean;
  c4db_endTransaction(db: Database, commit: boolean): boolean;

  kC4InfoStore: string;
  kC4LocalDocStore: string;

  c4raw_get(db: Database, storeID: string, docID: string): RawDocument;
  c4raw_put(
    db: Database,
    storeID: string,
    docID: string,
    meta: string,
    body: string
  ): boolean;

  c4_dumpInstances(): void;
  c4_getObjectCount(): number;

  c4doc_get(db: Database, docID: string, mustExist: boolean): Document;
  c4doc_put(document: Document): boolean;
  c4doc_save(document: Document, maxRevTreeDepth: number): boolean;

  // C4Document

  C4DocumentFlags: DocumentFlags;
  C4RevisionFlags: RevisionFlags;
}
