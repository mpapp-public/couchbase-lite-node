# couchbase-lite-node

This repository contains an experimental
[N-API](https://nodejs.org/api/n-api.html) based Node.js wrapper to
[Couchbase Lite Core](https://github.com/couchbase/couchbase-lite-coreboth).

The more appropriate name would perhaps be couchbase-lite-core-node given that
only a selection of the C4 prefixed C APIs are wrapped, but the goal here is to
proceed with Couchbase Lite APIs once the low level C4 APIs are sufficiently
covered.

## Build instructions

Building the couchbase-lite-node module has most recently been tested on Linux,
but both macOS and Linux build should work. If you want to make
[@mz2](http://github.com/mz2) happy, please help by adding Windows build
support!

### On macOS

```
git submodule update --init --recursive
cd vendor/couchbase-lite-core/build_cmake/scripts
./build_macos.sh # please read instructions at https://github.com/couchbase/couchbase-lite-coreboth to meet couchbase-lite-core prerequisites.
cd ../../../../
npm install
npm install -g node-gyp
node-gyp configure build
DYLD_LIBRARY_PATH=./vendor/couchbase-lite-core/build_cmake/macos npm test
```

### On Linux

```
git submodule update --init --recursive
cd vendor/couchbase-lite-core/build_cmake/scripts
./build_unix.sh # please read instructions at https://github.com/couchbase/couchbase-lite-coreboth to meet couchbase-lite-core prerequisites.
cd ../../../../
npm install
npm install -g node-gyp
node-gyp configure build
LD_LIBRARY_PATH=./vendor/couchbase-lite-core/build_cmake/unix npm test
```

## TODO

Here's the current status, roughly in intended implementation order:

* [x] Prove that the concept of wrapping couchbase-lite-core via the C4 C API
      and the N-API is sane (build & run tests to see for yourself).
* [x] Ensure that the C4 resource lifecycles are handled sanely (finalization
      callbacks fire to free C4 database / doc handles).
* [x] Test suite for the wrapped Node.js API.
* [x] Execute tests with GitLab's CI pipeline.
* [x] Create TypeScript type mappings for the API.

* [x] Support building on Mac.
* [x] Support building on Linux.
* [ ] Support building on Windows.
* [ ] Make the couchbase-lite-core package easily buildable using NPM & node-gyp
      alone (with prebuilt libLiteCore for supported platforms?).
* [ ] Create a Couchbase Lite like API for Node.js using the Swift (?) public
      API as a guide.

* [ ] Cover (most of) the couchbase-lite-core C API with a Node.js wrapper:
* [x] C4Base
* [x] C4Database
* [ ] C4Document
* [ ] C4Document+Fleece
* [ ] C4ExpiryEnumerator
* [ ] C4Observer
* [ ] C4Query
* [ ] C4Replicator
* [ ] C4Socket
