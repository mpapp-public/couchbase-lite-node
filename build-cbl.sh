#!/bin/bash

if [ ! -d ./vendor/couchbase-lite-core/build_cmake/scripts ]; then
    git submodule update --init --recursive
fi

pushd ./vendor/couchbase-lite-core/build_cmake/scripts

if [[ `uname` == 'Darwin' ]]; then
./build_macos.sh
else
./build_unix.sh
fi

popd

echo 'ok'