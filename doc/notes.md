# Notes for later

- If we were to add back node-addon reference:
  * need to add key `"dependencies:": ["<!(node -p \"require('node-addon-api').gyp\")"]` in binding.gyp
  * add to `include_dirs` the following entries: `"<!@(node -p \"require('node-addon-api').include\")"`
                              