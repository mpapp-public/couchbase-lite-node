import * as LibFleece from "./LibFleece";
import * as LibLiteCore from "./LibLiteCore";

export const libLiteCore: LibLiteCore.C4 = require("bindings")(
  "couchbaselite.node"
);

export const libFleece: LibFleece.Fleece = require("bindings")("fleece.node");

export default libLiteCore;
