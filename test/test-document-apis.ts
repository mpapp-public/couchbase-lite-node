#!/usr/bin/env node

const fs = require("fs");

import { libLiteCore } from "../index";
import { DatabaseConfiguration, EncryptionKey } from "../LibLiteCore";

const rimraf = require("rimraf").sync;
const tmp = require("tmp");
const fsExtra = require("fs-extra");
const path = require("path");

rimraf("./document-api-test-db");

test("Test getting data from an existing database", () => {
  const encryptionKey = {
    algorithm: libLiteCore.C4EncryptionAlgorithm.kC4EncryptionNone,
    bytes: Buffer.alloc(32)
  };

  const config: DatabaseConfiguration = {
    flags: 0,
    storageEngine: libLiteCore.kC4SQLiteStorageEngine,
    versioning: libLiteCore.C4DocumentVersioning.kC4RevisionTrees,
    encryptionKey: encryptionKey
  };

  const tmp_dir = tmp.dirSync({ keep: true });
  const tmp_db_path = path.join(tmp_dir.name, "netdb.cblite2");
  fsExtra.copySync(
    "./vendor/couchbase-lite-core/LiteCore/tests/data/replacedb/net130/netdb.cblite2",
    tmp_db_path
  );
  expect(fs.existsSync(tmp_db_path)).toBeTruthy();
  const db = libLiteCore.c4db_open(tmp_db_path, config);

  const doc1 = libLiteCore.c4doc_get(db, "doc1", false);
  expect(doc1).toBeTruthy();
  expect(doc1.revID()).toEqual("2-0648b6fe63bcc97db824a6d911b6aafc");
  expect(doc1.docID()).toEqual("doc1");

  /* tslint:disable:no-bitwise */
  const expectedDocFlags =
    libLiteCore.C4DocumentFlags.kDocExists |
    libLiteCore.C4DocumentFlags.kDocHasAttachments;
  const expectedRevFlags =
    libLiteCore.C4RevisionFlags.kRevLeaf |
    libLiteCore.C4RevisionFlags.kRevHasAttachments;
  /* tslint:enable:no-bitwise */

  expect(doc1.flags()).toEqual(expectedDocFlags);
  expect(doc1.sequence()).toEqual(1);

  const rev1 = doc1.selectedRev();
  // expect(rev1.body()).toEqual("foo");
  expect(rev1.flags()).toEqual(expectedRevFlags);
  expect(rev1.revID()).toEqual("2-0648b6fe63bcc97db824a6d911b6aafc");
  expect(rev1.sequence()).toEqual(1);

  libLiteCore.c4db_close(db);
});

test("Test getting and saving a document", () => {
  const encryptionKey: EncryptionKey = {
    algorithm: libLiteCore.C4EncryptionAlgorithm.kC4EncryptionNone,
    bytes: Buffer.alloc(32)
  };

  const config: DatabaseConfiguration = {
    flags: libLiteCore.C4DatabaseFlags.kC4DB_Create,
    storageEngine: libLiteCore.kC4SQLiteStorageEngine,
    versioning: libLiteCore.C4DocumentVersioning.kC4RevisionTrees,
    encryptionKey: encryptionKey
  };

  const db = libLiteCore.c4db_open("./document-api-test-db", config);

  expect(() => {
    libLiteCore.c4doc_get(db, "does-not-exist", true);
  }).toThrow();

  // if third argument (mustExist) is falsy, then a new empty document is created with this ID.
  const emptyDoc = libLiteCore.c4doc_get(db, "does-now-exist", false);
  expect(emptyDoc).toBeTruthy();

  expect(emptyDoc.flags()).toEqual(0);
  expect(emptyDoc.docID()).toEqual("does-now-exist");
  expect(emptyDoc.revID()).toBeFalsy();
  expect(emptyDoc.revID()).toBeFalsy();

  expect(emptyDoc.sequence()).toEqual(0);
  expect(emptyDoc.selectedRev()).toBeTruthy();
  expect(emptyDoc.selectedRev()).toBeTruthy(); // 2nd call comes from a cache

  const selectedRev = emptyDoc.selectedRev();
  expect(selectedRev.revID()).toBeFalsy();
  expect(selectedRev.flags()).toEqual(0);
  expect(selectedRev.sequence()).toEqual(0);
  expect(selectedRev.body()).toBeFalsy();

  expect(libLiteCore.c4db_beginTransaction(db)).toBeTruthy();
  expect(libLiteCore.c4doc_save(emptyDoc, 32)).toBeTruthy();
  expect(libLiteCore.c4db_endTransaction(db, true)).toBeTruthy();

  /// mustExist = true tested now for this document that got implicitly created above.
  // emptyDoc = libLiteCore.c4doc_get(db, "does-now-exist", true);

  expect(libLiteCore.c4db_close(db)).toBeTruthy();
  rimraf("./document-api-test-db");
});
